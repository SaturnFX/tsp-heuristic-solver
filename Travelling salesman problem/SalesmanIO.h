#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "LinkedList.h"

/*
* Modu� SalesmanIO
*
* Zawiera funkcje umo�liwiaj�ce �adowanie lub
* prezentowanie danych zwi�zanych z algortmami
* rozwi�zuj�cymi problem aTSP
*/

//Metoda wy�wietlaj�ca w konsoli dane wej�ciowe
//algorytmu komiwoja�era
void PrintSalesmanCities(const SalesmanCities& InputData);
//Metoda wy�wietlaj�ca tras�, b�d�c� rozwi�zaniem
//problemu komiwoja�era
void PrintSalesmanRoute(const LinkedList<int> InputRoute, int RouteDistance);
//Funkcja za�adowuj�ca z pliku tekstowego dane wej�ciowe
//do algorytm�w TSP
int LoadSalesmanCitiesFromFile(std::string FilePath, SalesmanCities*& OutputData);
//Funkcja za�adowuj�ca z pliku tekstowego dane wej�ciowe
//do algorytm�w TSP w formacie u�ywanym przez bibliotek� TSPLIB
int LoadSalesmanCitiesFromTSPLIBFile(std::string FilePath, SalesmanCities*& OutputData);
//Metoda wype�niaj�ca losowymi danymi istniej�c� struktur� 
//opisuj�c� dystanse mi�dzy miastami dla problemu TSP
void FillSalesmanCitiesWithRandomDistances(SalesmanCities& InputData,
	int MinimumDistance, int MaximumDistance);