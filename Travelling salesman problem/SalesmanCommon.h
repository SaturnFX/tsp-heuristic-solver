#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "LinkedList.h"
#include "Exception.h"

/*
* Modu� SalesmanCommon
*
* Zawiera funkcje wykorzystywane
* w wielu algorytmach rozwi�zuj�cych
* problem komiwoja�era
*/

//Funkcja wygenerowuj�ca tras� pocz�tkow� 
//dla heurystycznych algorytm�w komiwoja�era
//przy u�yciu algorytmu najbli�szego s�siada
int GetRouteArrayUsingNNA(const SalesmanCities& InputData, 
	int StartCity, int* RouteArray);

//Funkcja dzia�aj�ca podobnie jak ta zadeklarowana
//powy�ej, ale zwracaj�ca wynik w postaci 2 tablic
//po��cze� miast - jednej wskazuj�cej na nast�pne
//miasta, a drugiej wskazuj�cej na poprzednie
int GetRouteTwoWayConnectionsUsingNNA(const SalesmanCities& InputData,
	int StartCity, int* ForwardConnections, int* BackwardConnections);

//Funkcja konwertuj�ca tablic� po��cze�
//trasy na list� miast
inline void ConvertRouteConnectionsToList(const int* CitiesConnection, 
	LinkedList<int>& OutputRoute)
{
	OutputRoute.AddLast(0);
	int CurrentCity = CitiesConnection[0];
	while (CurrentCity)
	{
		OutputRoute.AddLast(CurrentCity);
		CurrentCity = CitiesConnection[CurrentCity];
	}
	OutputRoute.AddLast(0);
}