#pragma once
#include "Global.h"

#include <random>

/*
* Modu� Utilities
*
* Zawiera procedury i zmienne pomocnicze
*/

//Zmienna generatora liczb pseudolosowych
extern std::default_random_engine* RandomGenerator;

//Procedura zamiany elementw
template <typename T>
void SwapItems(T* ArrayData, int FirstIndex, int SecondIndex)
{
	T TemporaryValue = ArrayData[FirstIndex];
	ArrayData[FirstIndex] = ArrayData[SecondIndex];
	ArrayData[SecondIndex] = TemporaryValue;
}

//Funkcja zwracaj�ca warto�� mniejsz� z 2 podanych
template <typename T>
inline T GetMinimumValue(T FirstValue, T SecondValue)
{
	return (FirstValue > SecondValue) ? SecondValue : FirstValue;
}

//Funkcja zwracaj�ca liczb� sekund, kt�re up�yn�y
//od p�nocy 1 stycznia 1970 roku
int64_t GetUNIXTimestamp();

//Funkcja zwracaj�ca liczb� milisekund, kt�re up�yn�y
//od p�nocy 1 stycznia 1970 roku
int64_t GetUNIXMiliseconds();