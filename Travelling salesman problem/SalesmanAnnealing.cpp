#include "Global.h"
#include "SalesmanAnnealing.h"
#include "SalesmanCommon.h"
#include "SalesmanIO.h"
#include "IndexedPriorityQueue.h"
#include "Utilities.h"

#include <cmath>
#include <limits>

//Pr�g temperatury poni�ej kt�rego ko�czone jest dzia�anie
//algorytmu, dobrane tak by szansa przyj�cia trasy 
//gorszej o 1 by�a zawsze wi�ksza od 0.0000453999
//https://www.wolframalpha.com/input/?i=e%5E%28-1%2F0.1%29
#define SA_TEMPERATURE_LIMIT 0.1
//Liczba losowanych punkt�w w obr�bie s�siedztwa
//dla ka�dej iteracji zewn�trznej p�tli algorytmu
#define SA_NEIGHBORHOOD_FRACTION(N) (N * (N - 1)) / 200

//Parametry dotycz�ce kalibracji algorytmu
#define SA_CALIBRATION_SECONDS 30
#define SA_CALIBRATION_TEMPERATURE_STEP 0.999999
#define SA_CALIBRATION_RAND_MININIM 0
#define SA_CALIBRATION_RAND_MAXIMUM 1000

//W�a�ciwa funkcja zawieraj�ca implementacj� SW
int SalesmanAnnealingHeurSolver(const SalesmanCities& InputData, 
	double TemperatureStep, int SecondsLimit, LinkedList<int>& OutputRoute,
	long double* ProcessingSpeed);
//Funkcja obliczaj�ca temperatur� pocz�tkow� dla SW
inline long double CalculateInitialTemperature(const SalesmanCities& InputData, 
	int NeighborhoodIterations, double TemperatureStep, int SecondsLimit);
//Funkcja obliczaj� wag� minimalnego drzewa spinaj�cego dla grafu
//wej�ciowego aTSP (wykorzystuj�c jego nieskierowan� interpretacj�).
//Jest ona zrealizowana za pomoc� zmodyfikowanego algorytmu Prima
int CalculateMSTWeightUsingPrim(const SalesmanCities& InputData);

long double IterationsPerSecond = 0.0;
int IterationsPerSecondSize = -1;

int SalesmanAnnealingHeurSolver(const SalesmanCities& InputData, 
	double TemperatureStep, int SecondsLimit, LinkedList<int>& OutputRoute)
{
	return SalesmanAnnealingHeurSolver(InputData, 
		TemperatureStep, SecondsLimit, OutputRoute, nullptr);
}

void CalibrateSalesmanAnnealing(int CitiesCount)
{
	LinkedList<int> CalibrationRoute;
	SalesmanCities CalibrationInput(CitiesCount);
	long double OutputSpeed = 0.0;
	FillSalesmanCitiesWithRandomDistances(CalibrationInput, 
		SA_CALIBRATION_RAND_MININIM, SA_CALIBRATION_RAND_MAXIMUM);
	SalesmanAnnealingHeurSolver(CalibrationInput, 
		SA_CALIBRATION_TEMPERATURE_STEP, 
		SA_CALIBRATION_SECONDS, CalibrationRoute, &OutputSpeed);
	IterationsPerSecond = OutputSpeed;
	IterationsPerSecondSize = CitiesCount;
}

void ResetSalesmanAnnealingCalibration()
{
	IterationsPerSecond = 0.0;
	IterationsPerSecondSize = -1;
}

int SalesmanAnnealingHeurSolver(const SalesmanCities& InputData, 
	double TemperatureStep, int SecondsLimit, LinkedList<int>& OutputRoute,
	long double* ProcessingSpeed)
{
	int FirstCityIndex = 0;
	int SecondCityIndex = 0;
	int FirstCity = 0;
	int FirstPrevCity = 0;
	int FirstNextCity = 0;
	int SecondCity = 0;
	int SecondPrevCity = 0;
	int SecondNextCity = 0;
	int IterationIndex = 0;
	bool IsFirstBeforeSecond = false;
	bool IsSecondBeforeFirst = false;
	int CurrentDelta = 0;
	int IterationCount = 0;
	std::uniform_real_distribution<double> AcceptChanceDist(0.0, 1.0);
	std::uniform_int_distribution<int> CityIndexDist(0, InputData.CitiesCount - 1);

	int* MinimumRoute = new int[InputData.CitiesCount];
	int MinimumDistance = GetRouteArrayUsingNNA(
		InputData, CityIndexDist(*RandomGenerator), MinimumRoute);
	int* LastRoute = new int[InputData.CitiesCount];
	std::copy(MinimumRoute, MinimumRoute + InputData.CitiesCount, 
		LastRoute);
	int LastDistance = MinimumDistance;
	
	int NeighborhoodIterations = SA_NEIGHBORHOOD_FRACTION(InputData.CitiesCount);
	long double Temperature = CalculateInitialTemperature(InputData, 
		NeighborhoodIterations, TemperatureStep, SecondsLimit);

	//Deklaracja zeminnych s�u��cych do monitorowania
	//czasu wykonania algorytmu
	int64_t CurrentTimestamp = GetUNIXTimestamp();
	int64_t FinishTimestamp = CurrentTimestamp + SecondsLimit;
	do
	{
		for (IterationIndex = 0; IterationIndex < NeighborhoodIterations; ++IterationIndex)
		{
			//Wylosowanie dw�ch r�nych wierzcho�k�w
			//dla transformacji swap - zamiany
			FirstCityIndex = CityIndexDist(*RandomGenerator);
			do
			{
				SecondCityIndex = CityIndexDist(*RandomGenerator);
			}
			while (FirstCityIndex == SecondCityIndex);

			//Transformacja swap, przeprowadzana jest w czasie
			//sta�ym, do czego potrzebne jest pobranie informacji
			//o rozmieszczeniu wybranych miast w aktualnej trasie
			FirstCity = LastRoute[FirstCityIndex];
			if (FirstCityIndex != 0) FirstPrevCity = LastRoute[FirstCityIndex - 1];
			else FirstPrevCity = LastRoute[InputData.CitiesCount - 1];
			if (FirstCityIndex != (InputData.CitiesCount - 1))
				FirstNextCity = LastRoute[FirstCityIndex + 1];
			else FirstNextCity = LastRoute[0];

			SecondCity = LastRoute[SecondCityIndex];
			if (SecondCityIndex != 0) SecondPrevCity = LastRoute[SecondCityIndex - 1];
			else SecondPrevCity = LastRoute[InputData.CitiesCount - 1];
			if (SecondCityIndex != (InputData.CitiesCount - 1)) 
				SecondNextCity = LastRoute[SecondCityIndex + 1];
			else SecondNextCity = LastRoute[0];

			IsFirstBeforeSecond = (FirstNextCity == SecondCity);
			IsSecondBeforeFirst = (SecondNextCity == FirstCity);

			//Obliczenie zysku/straty z przyj�cia nowego rozwi�zania
			CurrentDelta = -InputData.CitiesDistances[FirstCity][FirstNextCity]
				- InputData.CitiesDistances[SecondCity][SecondNextCity];
			if (IsFirstBeforeSecond)
			{
				CurrentDelta += InputData.CitiesDistances[SecondCity][FirstCity];
			}
			else
			{
				CurrentDelta += (-InputData.CitiesDistances[SecondPrevCity][SecondCity]
					+ InputData.CitiesDistances[SecondCity][FirstNextCity]
					+ InputData.CitiesDistances[SecondPrevCity][FirstCity]);
			}
			if (IsSecondBeforeFirst)
			{
				CurrentDelta += InputData.CitiesDistances[FirstCity][SecondCity];
			}
			else
			{
				CurrentDelta += (-InputData.CitiesDistances[FirstPrevCity][FirstCity]
					+ InputData.CitiesDistances[FirstPrevCity][SecondCity]
					+ InputData.CitiesDistances[FirstCity][SecondNextCity]);
			}

			//Przyj�cie nowego rozwi�zania, gdy jest ono lepsze
			//lub, z pewnym prawdopodobie�stwem zale�nym 
			//od aktualnej temperatury
			if ((CurrentDelta < 0) || (AcceptChanceDist(*RandomGenerator) 
				< exp(-CurrentDelta / Temperature)))
			{
				LastRoute[FirstCityIndex] = SecondCity;
				LastRoute[SecondCityIndex] = FirstCity;
				LastDistance += CurrentDelta;
				if (LastDistance < MinimumDistance)
				{
					std::copy(LastRoute, LastRoute + InputData.CitiesCount, 
						MinimumRoute);
					MinimumDistance = LastDistance;
				}
			}
			++IterationCount;
		}
		//Sch�odzenie uk�adu
		Temperature *= TemperatureStep;
		CurrentTimestamp = GetUNIXTimestamp();
	}
	//Warunek zako�czenia algorytmu przyjmuj�cy warto�� prawda, gdy 
	//zostanie przekroczony czas lub zostanie osi�gni�ta temperatura
	//minimalna
	while ((CurrentTimestamp < FinishTimestamp) && (Temperature > SA_TEMPERATURE_LIMIT));
	//Ilo�� iteracji wykorzystywana podczas kalibracji algorytmu
	if (ProcessingSpeed)
	{
		*ProcessingSpeed = ((long double) IterationCount) 
			/ (SecondsLimit - FinishTimestamp + CurrentTimestamp) * 0.99;
	}
	delete[] LastRoute;
	OutputRoute.Load(MinimumRoute, InputData.CitiesCount);
	OutputRoute.AddLast(MinimumRoute[0]);
	delete[] MinimumRoute;
	return MinimumDistance;
}

long double CalculateInitialTemperature(const SalesmanCities& InputData, 
	int NeighborhoodIterations, double TemperatureStep, int SecondsLimit)
{
	long double CitiesMSTWeight = CalculateMSTWeightUsingPrim(InputData);
	//Sprawdzenie czy wykonana zosta�a kalibracja
	if (IterationsPerSecondSize != InputData.CitiesCount)
	{
		return CitiesMSTWeight;
	}
	long double TemperatureChange = pow((long double) TemperatureStep, 
		 (SecondsLimit * IterationsPerSecond) / NeighborhoodIterations);
	return GetMinimumValue(SA_TEMPERATURE_LIMIT / TemperatureChange, CitiesMSTWeight);
}

int CalculateMSTWeightUsingPrim(const SalesmanCities& InputData)
{
	int CurrentCity = 0;
	int NextCity = 0;
	int CurrentDistance = 0;
	int* CitiesDistances = new int[InputData.CitiesCount];
	bool* VisitStates = new bool[InputData.CitiesCount];
	for (CurrentCity = 0; CurrentCity < InputData.CitiesCount; ++CurrentCity)
	{
		CitiesDistances[CurrentCity] = std::numeric_limits<int>::max();
		VisitStates[CurrentCity] = false;
	}
	IndexedPriorityQueue<int> CitiesQueue(InputData.CitiesCount - 1);
	CitiesDistances[0] = 0;
	CitiesQueue.Add(0, 0);
	while (!CitiesQueue.IsEmpty())
	{
		CurrentCity = CitiesQueue.ExtractMinimumIndex();
		VisitStates[CurrentCity] = true;
		for (NextCity = 0; NextCity < InputData.CitiesCount; ++NextCity)
		{
			if (CurrentCity == NextCity)
			{
				continue;
			}
			if (VisitStates[NextCity]) 
			{
				continue;
			}
			//Zinterpretowanie skierowanego grafu po��cze�
			//jako nieskierowanego (wymaganego dla MST)
			//przez przyj�cie kr�tszej d�ugo�ci jako odleg�o�ci
			//pomi�dzy dwoma wierzcho�kami 
			CurrentDistance = GetMinimumValue(
				InputData.CitiesDistances[CurrentCity][NextCity], 
				InputData.CitiesDistances[NextCity][CurrentCity]);
			if (CurrentDistance >= CitiesDistances[NextCity]) 
			{
				continue;
			}
			CitiesDistances[NextCity] = CurrentDistance;
			if (CitiesQueue.Contains(NextCity))
			{
				CitiesQueue.Update(NextCity, CurrentDistance);
			}
			else
			{
				CitiesQueue.Add(NextCity, CurrentDistance);
			}
		}
	}
	delete[] VisitStates;
	int TotalWeight = 0;
	for (CurrentCity = 0; CurrentCity < InputData.CitiesCount; ++CurrentCity)
	{
		TotalWeight += CitiesDistances[CurrentCity];
	}
	delete[] CitiesDistances;
	return TotalWeight;
}