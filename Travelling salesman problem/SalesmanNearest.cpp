#include "Global.h"
#include "SalesmanNearest.h"
#include "SalesmanCommon.h"

int SalesmanNearestHeurSolver(const SalesmanCities& InputData, 
	int StartCity, LinkedList<int>& OutputRoute)
{
	int* RouteArray = new int[InputData.CitiesCount];
	int RouteDistance = GetRouteArrayUsingNNA(
		InputData, StartCity, RouteArray);
	OutputRoute.Load(RouteArray, InputData.CitiesCount);
	OutputRoute.AddLast(RouteArray[0]);
	delete[] RouteArray;
	return RouteDistance;
}