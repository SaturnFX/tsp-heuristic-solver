#include "Global.h"
#include "SalesmanData.h"

SalesmanCities::SalesmanCities(int InputCount)
	:CitiesCount(InputCount)
{
	CitiesDistances = new int*[InputCount];
	for (int CityIndex = 0; CityIndex < InputCount; ++CityIndex)
	{
		CitiesDistances[CityIndex] = new int[InputCount]();
		CitiesDistances[CityIndex][CityIndex] = -1;
	}
}

SalesmanCities::SalesmanCities(const SalesmanCities& ExistingCitiesData)
	:CitiesCount(ExistingCitiesData.CitiesCount)
{
	int* CurrentFromArray = nullptr;
	int* OriginalFromArray = nullptr;
	CitiesDistances = new int*[CitiesCount];
	for (int FromIndex = 0; FromIndex < CitiesCount; ++FromIndex)
	{
		CurrentFromArray = new int[CitiesCount];
		OriginalFromArray = ExistingCitiesData.CitiesDistances[FromIndex];
		CitiesDistances[FromIndex] = CurrentFromArray;
		for (int ToIndex = 0; ToIndex < CitiesCount; ++ToIndex)
		{
			CurrentFromArray[ToIndex] = OriginalFromArray[ToIndex];
		}
	}
}

SalesmanCities::~SalesmanCities()
{
	for (int CityIndex = 0; CityIndex < CitiesCount; ++CityIndex)
	{
		delete[] CitiesDistances[CityIndex];
	}
	delete[] CitiesDistances;
}