#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "LinkedList.h"
#include "Exception.h"

/*
* Modu� SalesmanGenetic
*
* Zawiera implementacje algorytmu genetycznego
* dla asymetrycznego problemu komiwoja�era,
* wykorzystuj�ce r�ne rozdzaje mutacji.
*/

//Algorytm genetyczny dla zamiany jako mutacji 
int SalesmanGeneticPMXSwapHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, int PopulationSize, double CrossingRate, 
	double MutationRate, LinkedList<int>& OutputRoute);

//Algorytm genetyczny dla wstawienia jako mutacji 
int SalesmanGeneticPMXInsertHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, int PopulationSize, double CrossingRate, 
	double MutationRate, LinkedList<int>& OutputRoute);