#include "Global.h"
#include "SalesmanIO.h"
#include "Utilities.h"

#include <iomanip>
#include <fstream>

#define TSPLIB_FILE_DIM_STRING "DIMENSION: "
#define TSPLIB_FILE_DIM_OFFSET 11
#define TSPLIB_FILE_DATA_STRING "EDGE_WEIGHT_SECTION"

void PrintSalesmanCities(const SalesmanCities& InputData)
{
	std::cout << "Liczba miast: " << InputData.CitiesCount << std::endl;
	std::cout << std::endl;
	std::cout << "         ";
	for (int CityIndex = 0; CityIndex < InputData.CitiesCount; ++CityIndex)
	{
		std::cout << std::setw(8) << CityIndex;
	}
	std::cout << std::endl << "        \xDA";
	for (int CityIndex = 0; CityIndex < InputData.CitiesCount; ++CityIndex)
	{
		std::cout << "\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4";
	}
	std::cout << std::endl;
	int* FromArray = nullptr;
	for (int FromIndex = 0; FromIndex < InputData.CitiesCount; ++FromIndex)
	{
		FromArray = InputData.CitiesDistances[FromIndex];
		std::cout << std::setw(8) << FromIndex << "\xB3";
		for (int ToIndex = 0; ToIndex < InputData.CitiesCount; ++ToIndex)
		{
			std::cout << std::setw(8) << FromArray[ToIndex];
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void PrintSalesmanRoute(const LinkedList<int> InputRoute, int RouteDistance)
{
	LinkedList<int>::ReadOnlyIterator CitiesIterator 
		= InputRoute.GetReadOnlyStartIterator();
	std::cout << *CitiesIterator++;
	while (!CitiesIterator.IsAtTheEnd())
	{
		std::cout << " -> " << *CitiesIterator;
		++CitiesIterator;
	}
	std::cout << std::endl;
	std::cout << "Ilo\x98\x86 miast na trasie: " << InputRoute.GetLength() << std::endl;
	std::cout << "\x9D\xA5""czny dystans trasy: " << RouteDistance << std::endl;
}

int LoadSalesmanCitiesFromFile(std::string FilePath, SalesmanCities*& OutputData)
{
	std::ifstream FileStream(FilePath);
	if (!FileStream.is_open()) return -1;
	int CitiesCount = 0;
	FileStream >> CitiesCount;
	OutputData = new SalesmanCities(CitiesCount);	
	int* FromArray = nullptr;
	int DiagonalDistance = 0;
	for (int FromIndex = 0; FromIndex < CitiesCount; ++FromIndex)
	{
		FromArray = OutputData->CitiesDistances[FromIndex];
		for (int ToIndex = 0; ToIndex < CitiesCount; ++ToIndex)
		{
			if (FromIndex != ToIndex)
			{
				FileStream >> FromArray[ToIndex];
			}
			else
			{
				FileStream >> DiagonalDistance;
			}
		}
	}
	FileStream.close();
	return CitiesCount;
}

int LoadSalesmanCitiesFromTSPLIBFile(std::string FilePath, SalesmanCities*& OutputData)
{
	std::ifstream FileStream(FilePath);
	if (!FileStream.is_open()) return -1;
	int CitiesCount = 0;
	std::string CurrentLine;
    do
	{
		std::getline(FileStream, CurrentLine);
	}
    while ((!FileStream.eof()) 
		&& (CurrentLine.find(TSPLIB_FILE_DIM_STRING) == std::string::npos));
	if (FileStream.eof())
	{
		FileStream.close();
		return -1;
	}
	CitiesCount = std::stoi(CurrentLine.substr(TSPLIB_FILE_DIM_OFFSET));
	do
	{
		std::getline(FileStream, CurrentLine);
	}
    while ((!FileStream.eof()) 
		&& (CurrentLine.find(TSPLIB_FILE_DATA_STRING) == std::string::npos));
	if (FileStream.eof())
	{
		FileStream.close();
		return -1;
	}
	OutputData = new SalesmanCities(CitiesCount);	
	int* FromArray = nullptr;
	int DiagonalDistance = 0;
	for (int FromIndex = 0; FromIndex < CitiesCount; ++FromIndex)
	{
		FromArray = OutputData->CitiesDistances[FromIndex];
		for (int ToIndex = 0; ToIndex < CitiesCount; ++ToIndex)
		{
			if (FromIndex != ToIndex)
			{
				FileStream >> FromArray[ToIndex];
			}
			else
			{
				FileStream >> DiagonalDistance;
			}
		}
	}
	FileStream.close();
	return CitiesCount;
}

void FillSalesmanCitiesWithRandomDistances(SalesmanCities& InputData,
	int MinimumDistance, int MaximumDistance)
{
	int* FromArray = nullptr;
	std::uniform_int_distribution<int> DistanceDist(MinimumDistance, MaximumDistance);
	for (int FromIndex = 0; FromIndex < InputData.CitiesCount; ++FromIndex)
	{
		FromArray = InputData.CitiesDistances[FromIndex];
		for (int ToIndex = 0; ToIndex < InputData.CitiesCount; ++ToIndex)
		{
			if (FromIndex != ToIndex)
			{
				FromArray[ToIndex] = DistanceDist(*RandomGenerator);
			}
		}
	}
}
