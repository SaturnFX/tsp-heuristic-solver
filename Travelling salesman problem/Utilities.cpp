#include "Global.h"
#include "Utilities.h"

#include <ctime> 

#define WIN32_LEAN_AND_MEAN
#include "Windows.h"

inline int64_t GetUNIXTimeInWindowsTicks();

std::default_random_engine* RandomGenerator = new std::default_random_engine(std::time(nullptr));

int64_t GetUNIXTimestamp()
{
	const int64_t TICKS_PER_SECOND = 10000000;
	return (GetUNIXTimeInWindowsTicks() / TICKS_PER_SECOND);
}

int64_t GetUNIXMiliseconds()
{
	const int64_t TICKS_PER_MILISECOND = 10000;
	return (GetUNIXTimeInWindowsTicks() / TICKS_PER_MILISECOND);
}

int64_t GetUNIXTimeInWindowsTicks()
{
	const int64_t TIMESTAMP_OFFSET = 0x19DB1DED53E8000;

	FILETIME CurrentTime;
	GetSystemTimeAsFileTime(&CurrentTime); 

	ULARGE_INTEGER IntegetStruct;
	IntegetStruct.LowPart = CurrentTime.dwLowDateTime;
	IntegetStruct.HighPart = CurrentTime.dwHighDateTime;

	return (IntegetStruct.QuadPart - TIMESTAMP_OFFSET);
}