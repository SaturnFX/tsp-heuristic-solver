#pragma once
#include "Global.h"

/*
* Klasa: AbstractContainer
*
* Klasa bazowa dla wszystkich klas
* b�d�cymi pojemnikami na jaki� rodzaj danych.
* Zawiera definicj� intefejsu
* kt�ry musi zaimplementowa� ka�da z nich.
*/
class AbstractContainer
{
protected:
	//Liczebno�� element�w w strukturze
	int DataLength;

	//Metoda umo�liwiaj�ca sprawdzenie
	//czy struktura zawiera jakiekoliek dane
	//je�li nie wyrzucany jest wyj�tek
	virtual void ValidateLength() const;
public:
	AbstractContainer();
	//Funkcja zwracaj�ca liczb� element�w w strukturze
	int GetLength() const;
	//Funkcja zwracaj�ca true je�li struktura jest pusta
	//a w przeciwnym przypadku false
	inline bool IsEmpty() const
	{
		return (GetLength() == 0);
	}
	//Metoda usuwaj�ca wszystkie elementy ze struktury
	virtual void Clear();
	virtual ~AbstractContainer();
};
