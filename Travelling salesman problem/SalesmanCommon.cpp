#include "Global.h"
#include "SalesmanCommon.h"

#include <limits>

int GetRouteArrayUsingNNA(const SalesmanCities& InputData,
	int StartCity, int* RouteArray)
{
	int TotalDistance = 0;
	int CurrentDistance = 0;
	int MinimumDistance = 0;
	int CurrentCity = 0;
	int MinimumCity = -1;
	int LastCity = StartCity;
	bool* VisitStates = new bool[InputData.CitiesCount]();
	RouteArray[0] = StartCity;
	VisitStates[StartCity] = true;
	for (int ArrayIndex = 1; ArrayIndex < InputData.CitiesCount; ++ArrayIndex)
	{
		MinimumDistance = std::numeric_limits<int>::max();
		MinimumCity = -1;
		for (CurrentCity = 0; CurrentCity < InputData.CitiesCount; ++CurrentCity)
		{
			if (CurrentCity == LastCity) continue;
			if (VisitStates[CurrentCity]) continue; 
			
			CurrentDistance = InputData.CitiesDistances[LastCity][CurrentCity];
			if (CurrentDistance < MinimumDistance)
			{
				MinimumDistance = CurrentDistance;
				MinimumCity = CurrentCity;
			}
		}
		if (MinimumCity < 0)
		{
			throw AlgorithmException("Next city could not be found somehow during NNA");
		}
		TotalDistance += MinimumDistance;
		RouteArray[ArrayIndex] = MinimumCity;
		VisitStates[MinimumCity] = true;
		LastCity = MinimumCity;
	}
	TotalDistance += InputData.CitiesDistances[LastCity][StartCity];
	delete[] VisitStates;
	return TotalDistance;
}

int GetRouteTwoWayConnectionsUsingNNA(const SalesmanCities& InputData,
	int StartCity, int* ForwardConnections, int* BackwardConnections)
{
	int TotalDistance = 0;
	int CurrentDistance = 0;
	int MinimumDistance = 0;
	int CurrentCity = 0;
	int MinimumCity = -1;
	int LastCity = StartCity;
	bool* VisitStates = new bool[InputData.CitiesCount]();
	VisitStates[StartCity] = true;
	for (int ArrayIndex = 1; ArrayIndex < InputData.CitiesCount; ++ArrayIndex)
	{
		MinimumDistance = std::numeric_limits<int>::max();
		MinimumCity = -1;
		for (CurrentCity = 0; CurrentCity < InputData.CitiesCount; ++CurrentCity)
		{
			if (CurrentCity == LastCity) continue;
			if (VisitStates[CurrentCity]) continue; 
			
			CurrentDistance = InputData.CitiesDistances[LastCity][CurrentCity];
			if (CurrentDistance < MinimumDistance)
			{
				MinimumDistance = CurrentDistance;
				MinimumCity = CurrentCity;
			}
		}
		if (MinimumCity < 0)
		{
			throw AlgorithmException("Next city could not be found somehow during NNA");
		}
		TotalDistance += MinimumDistance;
		ForwardConnections[LastCity] = MinimumCity;
		BackwardConnections[MinimumCity] = LastCity;
		VisitStates[MinimumCity] = true;
		LastCity = MinimumCity;
	}
	TotalDistance += InputData.CitiesDistances[LastCity][StartCity];
	ForwardConnections[LastCity] = StartCity;
	BackwardConnections[StartCity] = LastCity;
	delete[] VisitStates;
	return TotalDistance;
}
