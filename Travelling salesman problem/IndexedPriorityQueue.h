#pragma once
#include "Global.h"
#include "AbstractContainer.h"
#include "Utilities.h"

inline int GetParentIndex(int Index);
inline int GetLeftChildIndex(int Index);
inline int GetRightChildIndex(int Index);

/*
* Klasa: IndexedPriorityQueue
*
* Kolejka priorytetowa oparta na kopcu
* binarnym, z interfejsem umo�liwiaj�cym
* przypisanie do ka�dego elementu w kolejce
* indeksu, po kt�rym odbywa si� dost�p,
* a tak�e zmian� elementu pod danym indeksem
*/
template <typename T>
class IndexedPriorityQueue : public AbstractContainer
{
private:
	//Tablica kluczy po
	//kt�rych odbywa si� por�wnanie
	T* KeysArray;
	//Tablica pe�ni�ca rol� kopca
	int* HeapArray;
	//Tablica mapuj�ca indeksy
	//na pozycje element�w kopca
	int* IndexesArray;
	//Rozmiar kopca
	int HeapLength;

	//Metoda tworz�ca kopiec w oparciu
	//o indeks maksymalny
	void CreateHeap(int MaximumIndex)
	{
		HeapLength = MaximumIndex + 1;
		KeysArray = new T[HeapLength];
		HeapArray = new int[HeapLength];
		IndexesArray = new int[HeapLength];
		for (int ItemIndex = 0; ItemIndex < HeapLength; ++ItemIndex)
		{
			IndexesArray[ItemIndex] = -1;
		}
		*HeapArray = 0;
	}

	//Metoda naprawiaj�ca w�asno��
	//kopca przechodz�c w g�r�
	void RepairHeapIndexUp(int ItemIndex)
	{
		int CurrentIndex = ItemIndex;
		int ParentIndex = GetParentIndex(CurrentIndex);
		int ItemValueIndex = HeapArray[ItemIndex];
		int ParentValueIndex = HeapArray[ParentIndex];
		T ItemValue = KeysArray[ItemValueIndex];
		while ((CurrentIndex > 1) && (KeysArray[ParentValueIndex] > ItemValue))
		{
			IndexesArray[ParentValueIndex] = CurrentIndex;
			HeapArray[CurrentIndex] = ParentValueIndex;
			CurrentIndex = ParentIndex;
			ParentIndex = GetParentIndex(CurrentIndex);
			ParentValueIndex = HeapArray[ParentIndex];
		}
		IndexesArray[ItemValueIndex] = CurrentIndex;
		HeapArray[CurrentIndex] = ItemValueIndex;
	}

	//Metoda naprawiaj�ca w�asno��
	//kopca przechodz�c w d�
	void RepairHeapIndexDown(int ItemIndex)
	{
		int SmallestIndex = ItemIndex;
		int LeftIndex = GetLeftChildIndex(ItemIndex);
		int RightIndex = GetRightChildIndex(ItemIndex);
		if ((LeftIndex <= DataLength) && 
			(KeysArray[HeapArray[LeftIndex]] < KeysArray[HeapArray[SmallestIndex]]))
		{
			SmallestIndex = LeftIndex;
		}
		if ((RightIndex <= DataLength) && 
			(KeysArray[HeapArray[RightIndex]] < KeysArray[HeapArray[SmallestIndex]]))
		{
			SmallestIndex = RightIndex;
		}
		if (SmallestIndex != ItemIndex)
		{
			IndexesArray[HeapArray[SmallestIndex]] = ItemIndex;
			IndexesArray[HeapArray[ItemIndex]] = SmallestIndex;
			SwapItems<int>(HeapArray, SmallestIndex, ItemIndex);
			RepairHeapIndexDown(SmallestIndex);
		}
	}

public:
	IndexedPriorityQueue(int MaximumIndex)
	{
		CreateHeap(MaximumIndex);
	}

	//Funkcja pobieraj�ca indeks
	//z minimalnym kluczem
	int GetMinimumIndex() const
	{
		ValidateLength();
		return HeapArray[1];
	}

	//Funkcja pobieraj�ca 
	//i usuwaj�ca indeks
	//z minimalnym kluczem
	int ExtractMinimumIndex()
	{
		ValidateLength();
		int MinimumIndex = HeapArray[1];
		IndexesArray[HeapArray[DataLength]] = 1;
		SwapItems<int>(HeapArray, 1, DataLength);
		--DataLength;
		IndexesArray[MinimumIndex] = -1;
		RepairHeapIndexDown(1);
		return MinimumIndex;
	}

	//Funkcja sprawdzaj�ca czy
	//dany indeks zawiera si�
	//w kolejce
	bool Contains(int Index)
	{
		return (IndexesArray[Index] > 0);
	}

	//Metoda dodaj�ca nowy indeks
	//o danym kluczu do kolejki
	void Add(int Index, T Value)
	{
		int ItemIndex = ++DataLength;
		IndexesArray[Index] = ItemIndex;
		HeapArray[ItemIndex] = Index;
		KeysArray[Index] = Value;
		RepairHeapIndexUp(ItemIndex);
	}

	//Metoda aktualizuj�ca klucz
	//zwi�zany z danym indeksem
	void Update(int Index, T Value)
	{
		KeysArray[Index] = Value;
		RepairHeapIndexUp(IndexesArray[Index]);
		RepairHeapIndexDown(IndexesArray[Index]);
	}

	//Metoda usuwaj�ca element
	//o danym indeksie
	void Remove(int Index)
	{
		int ValueIndex = IndexesArray[Index];
		IndexesArray[HeapArray[DataLength]] = ValueIndex;
		SwapItems<int>(HeapArray, ValueIndex, DataLength);
		--DataLength;
		IndexesArray[Index] = -1;
		RepairHeapIndexDown(ValueIndex);
	}

	//Metoda czyszcz�ca kolejk�
	void Clear()
	{
		delete[] IndexesArray;
		delete[] HeapArray;
		delete[] KeysArray;
		CreateHeap(HeapLength - 1);
		AbstractContainer::Clear();
	}

	~IndexedPriorityQueue()
	{
		delete[] IndexesArray;
		delete[] HeapArray;
		delete[] KeysArray;
	}
};

inline int GetParentIndex(int Index)
{
	return Index / 2;
}

inline int GetLeftChildIndex(int Index) 
{
	return 2 * Index;
}

inline int GetRightChildIndex(int Index)
{
	return 2 * Index + 1;
}
