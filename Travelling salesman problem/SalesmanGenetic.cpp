#include "Global.h"
#include "SalesmanGenetic.h"
#include "SalesmanCommon.h"
#include "Utilities.h"

//Struktura opisuj�ca pojedy�czego
//osobnika algorytmu genetycznego
//dla problemu aTSP
struct SalesmanIndividual
{
	int FitnessValue;
	int* Chromosome;

	SalesmanIndividual(int ChromosomeSize);
	inline void CopyFrom(const SalesmanIndividual& Base, 
		int ChromosomeSize);
	//Za�adowanie rozwi�zania z algorytmu 
	//najbli�szego s�siada
	inline void PutNearestNeighbourSolution(
		const SalesmanCities& InputData, int StartCity);
	//Za�adowanie losowego rozwi�zania
	void Randomize(const SalesmanCities& InputData);
	//Zaktualizowanie warto�ci przystosowania
	void UpdateFitness(const SalesmanCities& InputData);
	~SalesmanIndividual();
};

int SalesmanGeneticPMXHeurSolver(
	const SalesmanCities& InputData, 
	int SecondsLimit, 
	int PopulationSize, 
	double CrossingRate,
	double MutationRate, 
	void (*MutationOperator)(
		const SalesmanCities&,
		std::uniform_int_distribution<int>&,
		SalesmanIndividual&), 
	LinkedList<int>& OutputRoute);

inline void CrossIndividualsUsingPMX(
	const SalesmanCities& InputData,
	const SalesmanIndividual& FirstParent,
	const SalesmanIndividual& SecondParent,
	SalesmanIndividual& FirstChild,
	SalesmanIndividual& SecondChild,
	int* FirstMapping,
	int* SecondMapping);

void MutateIndividualUsingSwap(
	const SalesmanCities& InputData,
	std::uniform_int_distribution<int>& GeneDist,
	SalesmanIndividual& InputIndividual);

void MutateIndividualUsingInsert(
	const SalesmanCities& InputData,
	std::uniform_int_distribution<int>& GeneDist,
	SalesmanIndividual& InputIndividual);

//Procedura sortowania populacji zaimplementowana
//z wykorzystaniem algorytmu QuickSort
void PerformIndividualsSorting(SalesmanIndividual** IndividualsArray, 
	int StartIndex, int EndIndex);

int PerformDivision(SalesmanIndividual** IndividualsArray, 
	int StartIndex, int EndIndex);

void PerformPopulationsMerge(
	SalesmanIndividual** NewPopulation, 
	SalesmanIndividual* const* OldPopulation,
	int SurvivePortionLength,
	int PopulationSize,
	SalesmanIndividual** OutputPopulation);

int SalesmanGeneticPMXSwapHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, int PopulationSize, double CrossingRate, 
	double MutationRate, LinkedList<int>& OutputRoute)
{
	return SalesmanGeneticPMXHeurSolver(InputData, SecondsLimit, 
		PopulationSize, CrossingRate, MutationRate, 
		&MutateIndividualUsingSwap, OutputRoute);
}

int SalesmanGeneticPMXInsertHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, int PopulationSize, double CrossingRate, 
	double MutationRate, LinkedList<int>& OutputRoute)
{
	return SalesmanGeneticPMXHeurSolver(InputData, SecondsLimit, 
		PopulationSize, CrossingRate, MutationRate, 
		&MutateIndividualUsingInsert, OutputRoute);
}

int SalesmanGeneticPMXHeurSolver(
	const SalesmanCities& InputData, 
	int SecondsLimit, 
	int PopulationSize, 
	double CrossingRate,
	double MutationRate, 
	void (*MutationOperator)(
		const SalesmanCities&,
		std::uniform_int_distribution<int>&,
		SalesmanIndividual&), 
	LinkedList<int>& OutputRoute)
{
	int IndividualIndex = 0;
	int SelectedIndex = 0;
	int LastSelectedIndex = 0;
	int FirstCrossingIndex = 0;
	int SecondCrossingIndex = 0;
	int BestIndividualIndex = 0;
	int LastIndividualIndex = PopulationSize - 1;
	int NearestIndividualsCount = GetMinimumValue(InputData.CitiesCount, PopulationSize);
	int SelectionPoolSize = (PopulationSize + 3) * 3 / 4;
	int SurvivePopulationPortion = PopulationSize / 5;
	int CurrentFitnessValue = 0;
	int BestFitnessValue = 0;
	long long int PopulationFitness = 0;
	long long int SelectionFitness = 0;
	double SelectProbability = 0.0;

	std::uniform_real_distribution<double> EventIndicatorDist(0.0, 1.0);
	std::uniform_int_distribution<int> CrossingIndividualDist(0, SelectionPoolSize - 1);
	std::uniform_int_distribution<int> GeneDist(0, InputData.CitiesCount - 1);
	SalesmanIndividual* CurrentIndividual = nullptr;
	SalesmanIndividual* CandidateIndividual = nullptr;
	SalesmanIndividual** CurrentPopulation = new SalesmanIndividual*[PopulationSize]();
	//Tablica zawieraj�ca now� populacj� osobnik�w zawieraja jeden dodatkowy
	//element tak, aby przy podaniu nieparzystej liczebno�ci populacji
	//mo�na by�o krzy�uj�c osobniki wygenerowa� liczb� par pokrywaj�c�
	//j� ca��. Ostatni element jest u�ywany tak�e jako tymczasowy.
	SalesmanIndividual** NewPopulation = new SalesmanIndividual*[PopulationSize + 1]();
	SalesmanIndividual** IndividualsArray = new SalesmanIndividual*[PopulationSize]();
	SalesmanIndividual** IndividualArrayPointer = nullptr;

	int* FirstAuxArray = new int[InputData.CitiesCount];
	int* SecondAuxArray = new int[InputData.CitiesCount];

	for (; IndividualIndex < NearestIndividualsCount; ++IndividualIndex)
	{
		CurrentIndividual = new SalesmanIndividual(InputData.CitiesCount);
		CurrentIndividual->PutNearestNeighbourSolution(InputData, IndividualIndex);
		PopulationFitness += CurrentIndividual->FitnessValue;
		CurrentPopulation[IndividualIndex] = CurrentIndividual;
		NewPopulation[IndividualIndex] = new SalesmanIndividual(InputData.CitiesCount);
	}
	for (; IndividualIndex < PopulationSize; ++IndividualIndex)
	{
		CurrentIndividual = new SalesmanIndividual(InputData.CitiesCount);
		CurrentIndividual->Randomize(InputData);
		PopulationFitness += CurrentIndividual->FitnessValue;
		CurrentPopulation[IndividualIndex] = CurrentIndividual;
		NewPopulation[IndividualIndex] = new SalesmanIndividual(InputData.CitiesCount);
	}
	NewPopulation[PopulationSize] = new SalesmanIndividual(InputData.CitiesCount);
	PerformIndividualsSorting(CurrentPopulation, 0, LastIndividualIndex);

	int64_t CurrentTimestamp = GetUNIXTimestamp();
	int64_t FinishTimestamp = CurrentTimestamp + SecondsLimit;
	do
	{
		//Selekcja puli osobnik�w do krzy�owania
		//oparta na metodzie ruletki
		std::copy(CurrentPopulation, CurrentPopulation 
			+ PopulationSize, IndividualsArray);
		SelectionFitness = PopulationFitness;
		for (LastSelectedIndex = 0; LastSelectedIndex < SelectionPoolSize; ++LastSelectedIndex)
		{
			SelectedIndex = -1;
			SelectProbability = 0.0;
			for (IndividualIndex = LastSelectedIndex; IndividualIndex < LastIndividualIndex; ++IndividualIndex)
			{
				SelectProbability += 
					(((double) IndividualsArray[IndividualIndex]
						->FitnessValue)	/ SelectionFitness);
				if (EventIndicatorDist(*RandomGenerator) <= SelectProbability)
				{
					SelectedIndex = IndividualIndex;
					break;
				}
			}
			if (SelectedIndex < 0)
			{
				SelectedIndex = LastIndividualIndex;
			}
			SelectionFitness -= IndividualsArray[SelectedIndex]->FitnessValue;
			SwapItems(IndividualsArray, SelectedIndex, LastSelectedIndex);
		}

		//Krzy�owanie osobnik�w z puli
		IndividualIndex = 0;
		while (IndividualIndex < PopulationSize)
		{
			FirstCrossingIndex = CrossingIndividualDist(*RandomGenerator);
			do
			{
				SecondCrossingIndex = CrossingIndividualDist(*RandomGenerator);
			}
			while (FirstCrossingIndex == SecondCrossingIndex);

			if (EventIndicatorDist(*RandomGenerator) < CrossingRate)
			{
				CrossIndividualsUsingPMX(InputData, 
					*IndividualsArray[FirstCrossingIndex],
					*IndividualsArray[SecondCrossingIndex],
					*NewPopulation[IndividualIndex],
					*NewPopulation[IndividualIndex + 1],
					FirstAuxArray, SecondAuxArray);
				IndividualIndex += 2;
			}
		}

		//Zastosowanie tzw. elityzmu - najlepszy osobnik jest specjalnie
		//chroniony przed pogorszeniem si� jego dopasowania w wyniku mutacji
		//
		//Znalezienie najlepszego elementu i przeniesienie go na pocz�tek tablicy
		BestIndividualIndex = 0;
		BestFitnessValue = NewPopulation[0]->FitnessValue;
		for (IndividualIndex = 1; IndividualIndex <= PopulationSize; ++IndividualIndex)
		{
			CurrentFitnessValue = NewPopulation[IndividualIndex]->FitnessValue;
			if (CurrentFitnessValue < 0)
			{
				continue;
			}
			if (CurrentFitnessValue < BestFitnessValue)
			{
				BestIndividualIndex = IndividualIndex;
				BestFitnessValue = CurrentFitnessValue;
			}
		}
		SwapItems(NewPopulation, BestIndividualIndex, 0);
		//Skopiowanie najlepszego elementu 
		NewPopulation[PopulationSize]->CopyFrom(*NewPopulation[0], InputData.CitiesCount);
		//Obowi�zkowa mutacja dla jego kopii
		MutateIndividualUsingSwap(InputData, GeneDist, *NewPopulation[PopulationSize]);
		//Przeprowadzenie mutacji reszty element�w
		for (IndividualIndex = 1; IndividualIndex < PopulationSize; ++IndividualIndex)
		{
			if (EventIndicatorDist(*RandomGenerator) < MutationRate)
			{
				MutationOperator(InputData, GeneDist, *NewPopulation[IndividualIndex]);
			}
		}

		//Posortowanie osobnik�w w nowej populacji
		PerformIndividualsSorting(NewPopulation, 0, PopulationSize);

		//Stworzenie kolejnej populacji z osobnik�w powsta�ych z krzy�owania
		//oraz mutacji, a tak�e z pewnej cz�ci najlepszych osobnik�w ze starej populacji
		PerformPopulationsMerge(NewPopulation, CurrentPopulation, 
			SurvivePopulationPortion, PopulationSize, IndividualsArray);

		IndividualArrayPointer = CurrentPopulation;
		CurrentPopulation = IndividualsArray;
		IndividualsArray = IndividualArrayPointer;

		PopulationFitness = 0;
		for (IndividualIndex = 0; IndividualIndex < PopulationSize; ++IndividualIndex)
		{
			PopulationFitness += CurrentPopulation[IndividualIndex]->FitnessValue;
		}

		CurrentTimestamp = GetUNIXTimestamp();
	}
	//Kryterium czasowe jako warunek zatrzymania algorytmu
	while (CurrentTimestamp < FinishTimestamp);

	CurrentIndividual = CurrentPopulation[0];
	int RouteLength = CurrentIndividual->FitnessValue;
	OutputRoute.Load(CurrentIndividual->Chromosome, InputData.CitiesCount);
	OutputRoute.AddLast(CurrentIndividual->Chromosome[0]);
	delete NewPopulation[PopulationSize];
	for (IndividualIndex = 0; IndividualIndex < PopulationSize; ++IndividualIndex)
	{
		delete NewPopulation[IndividualIndex];
		delete CurrentPopulation[IndividualIndex];
	}
	delete[] FirstAuxArray;
	delete[] SecondAuxArray;
	delete[] IndividualsArray;
	delete[] NewPopulation;
	delete[] CurrentPopulation;
	return RouteLength;
}

void CrossIndividualsUsingPMX(const SalesmanCities& InputData,
	const SalesmanIndividual& FirstParent,
	const SalesmanIndividual& SecondParent,
	SalesmanIndividual& FirstChild,
	SalesmanIndividual& SecondChild,
	int* FirstMapping,
	int* SecondMapping)
{
	int CityIndex = 0;
	int FirstParentCity = 0;
	int SecondParentCity = 0;
	int FirstMappingIndex = 0;
	int SecondMappingIndex = 0;
	int FirstMappingTarget = 0;
	int SecondMappingTarget = 0;
	int FirstDivisionIndex = 
		std::uniform_int_distribution<int>(0, 
		(InputData.CitiesCount - 1) / 2)(*RandomGenerator);
	int SecondDivisionIndex = 
		std::uniform_int_distribution<int>((InputData.CitiesCount / 2) + 1,
		InputData.CitiesCount - 1)(*RandomGenerator);
	//Wype�nianie tablic mapowania i przepisywanie �rodkowej
	//cz�ci trasy do tras potomnych
	for (; CityIndex < InputData.CitiesCount; ++CityIndex)
	{
		FirstMapping[CityIndex] = -1;
		SecondMapping[CityIndex] = -1;
	}
	for (CityIndex = FirstDivisionIndex; CityIndex < SecondDivisionIndex; ++CityIndex)
	{
		FirstParentCity = FirstParent.Chromosome[CityIndex];
		SecondParentCity = SecondParent.Chromosome[CityIndex];
		FirstChild.Chromosome[CityIndex] = SecondParentCity;
		SecondChild.Chromosome[CityIndex] = FirstParentCity;

		FirstMappingIndex = SecondParentCity;
		SecondMappingIndex = FirstParentCity;
		FirstMappingTarget = FirstMapping[FirstParentCity];
		SecondMappingTarget = SecondMapping[SecondParentCity];
		if (FirstMappingTarget < 0)
		{
			FirstMappingTarget = FirstParentCity;
		}
		else
		{
			SecondMappingIndex = FirstMappingTarget;
		}
		if (SecondMappingTarget < 0)
		{
			SecondMappingTarget = SecondParentCity;
		}
		else
		{
			FirstMappingIndex = SecondMappingTarget;
		}
		FirstMapping[FirstMappingIndex] = FirstMappingTarget;
		SecondMapping[SecondMappingIndex] = SecondMappingTarget;
	}
	//Przepisywanie pozosta�ych cz�ci tras z uwzgl�dnieniem mapowania
	for (CityIndex = 0; CityIndex < FirstDivisionIndex; ++CityIndex)
	{
		FirstParentCity = FirstParent.Chromosome[CityIndex];
		SecondParentCity = SecondParent.Chromosome[CityIndex];
		FirstMappingTarget = FirstMapping[FirstParentCity];
		SecondMappingTarget = SecondMapping[SecondParentCity];
		if (FirstMappingTarget < 0)
		{
			FirstChild.Chromosome[CityIndex] = FirstParentCity;
		}
		else
		{
			FirstChild.Chromosome[CityIndex] = FirstMappingTarget;
		}
		if (SecondMappingTarget < 0)
		{
			SecondChild.Chromosome[CityIndex] = SecondParentCity;
		}
		else
		{
			SecondChild.Chromosome[CityIndex] = SecondMappingTarget;
		}
	}
	for (CityIndex = SecondDivisionIndex; CityIndex < InputData.CitiesCount; ++CityIndex)
	{
		FirstParentCity = FirstParent.Chromosome[CityIndex];
		SecondParentCity = SecondParent.Chromosome[CityIndex];
		FirstMappingTarget = FirstMapping[FirstParentCity];
		SecondMappingTarget = SecondMapping[SecondParentCity];
		if (FirstMappingTarget < 0)
		{
			FirstChild.Chromosome[CityIndex] = FirstParentCity;
		}
		else
		{
			FirstChild.Chromosome[CityIndex] = FirstMappingTarget;
		}
		if (SecondMappingTarget < 0)
		{
			SecondChild.Chromosome[CityIndex] = SecondParentCity;
		}
		else
		{
			SecondChild.Chromosome[CityIndex] = SecondMappingTarget;
		}
	}
	FirstChild.UpdateFitness(InputData);
	SecondChild.UpdateFitness(InputData);
}

void MutateIndividualUsingSwap(const SalesmanCities& InputData,
	std::uniform_int_distribution<int>& GeneDist,
	SalesmanIndividual& InputIndividual)
{
	int FirstCity = 0;
	int FirstPrevCity = 0;
	int FirstNextCity = 0;
	int SecondCity = 0;
	int SecondPrevCity = 0;
	int SecondNextCity = 0;

	int FirstCityIndex = GeneDist(*RandomGenerator);
	int SecondCityIndex = 0;
	do
	{
		SecondCityIndex = GeneDist(*RandomGenerator);
	}
	while (FirstCityIndex == SecondCityIndex);

	FirstCity = InputIndividual.Chromosome[FirstCityIndex];
	if (FirstCityIndex != 0) FirstPrevCity = InputIndividual.Chromosome[FirstCityIndex - 1];
	else FirstPrevCity = InputIndividual.Chromosome[InputData.CitiesCount - 1];
	if (FirstCityIndex != (InputData.CitiesCount - 1))
		FirstNextCity = InputIndividual.Chromosome[FirstCityIndex + 1];
	else FirstNextCity = InputIndividual.Chromosome[0];

	SecondCity = InputIndividual.Chromosome[SecondCityIndex];
	if (SecondCityIndex != 0) SecondPrevCity = InputIndividual.Chromosome[SecondCityIndex - 1];
	else SecondPrevCity = InputIndividual.Chromosome[InputData.CitiesCount - 1];
	if (SecondCityIndex != (InputData.CitiesCount - 1)) 
		SecondNextCity = InputIndividual.Chromosome[SecondCityIndex + 1];
	else SecondNextCity = InputIndividual.Chromosome[0];

	bool IsFirstBeforeSecond = (FirstNextCity == SecondCity);
	bool IsSecondBeforeFirst = (SecondNextCity == FirstCity);

	int FitnessDelta = -InputData.CitiesDistances[FirstCity][FirstNextCity]
		- InputData.CitiesDistances[SecondCity][SecondNextCity];
	if (IsFirstBeforeSecond)
	{
		FitnessDelta += InputData.CitiesDistances[SecondCity][FirstCity];
	}
	else
	{
		FitnessDelta += (-InputData.CitiesDistances[SecondPrevCity][SecondCity]
			+ InputData.CitiesDistances[SecondCity][FirstNextCity]
			+ InputData.CitiesDistances[SecondPrevCity][FirstCity]);
	}
	if (IsSecondBeforeFirst)
	{
		FitnessDelta += InputData.CitiesDistances[FirstCity][SecondCity];
	}
	else
	{
		FitnessDelta += (-InputData.CitiesDistances[FirstPrevCity][FirstCity]
			+ InputData.CitiesDistances[FirstPrevCity][SecondCity]
			+ InputData.CitiesDistances[FirstCity][SecondNextCity]);
	}
	InputIndividual.Chromosome[FirstCityIndex] = SecondCity;
	InputIndividual.Chromosome[SecondCityIndex] = FirstCity;
	InputIndividual.FitnessValue += FitnessDelta;
}

void MutateIndividualUsingInsert(const SalesmanCities& InputData,
	std::uniform_int_distribution<int>& GeneDist,
	SalesmanIndividual& InputIndividual)
{
	int FirstCity = 0;
	int FirstPrevCity = 0;
	int FirstNextCity = 0;
	int SecondCity = 0;
	int SecondPrevCity = 0;
	int SecondNextCity = 0;
	
	int FirstCityIndex = GeneDist(*RandomGenerator);
	int SecondCityIndex = 0;
	do
	{
		SecondCityIndex = GeneDist(*RandomGenerator);
	}
	while (FirstCityIndex == SecondCityIndex);

	FirstCity = InputIndividual.Chromosome[FirstCityIndex];
	if (FirstCityIndex != 0) FirstPrevCity = InputIndividual.Chromosome[FirstCityIndex - 1];
	else FirstPrevCity = InputIndividual.Chromosome[InputData.CitiesCount - 1];
	if (FirstCityIndex != (InputData.CitiesCount - 1))
		FirstNextCity = InputIndividual.Chromosome[FirstCityIndex + 1];
	else FirstNextCity = InputIndividual.Chromosome[0];

	SecondCity = InputIndividual.Chromosome[SecondCityIndex];
	if (SecondCityIndex != 0) SecondPrevCity = InputIndividual.Chromosome[SecondCityIndex - 1];
	else SecondPrevCity = InputIndividual.Chromosome[InputData.CitiesCount - 1];

	if (SecondCity == FirstNextCity) return;

	int FitnessDelta = -InputData.CitiesDistances[FirstPrevCity][FirstCity]
		- InputData.CitiesDistances[FirstCity][FirstNextCity]
		- InputData.CitiesDistances[SecondPrevCity][SecondCity]
		+ InputData.CitiesDistances[FirstPrevCity][FirstNextCity]
		+ InputData.CitiesDistances[SecondPrevCity][FirstCity]
		+ InputData.CitiesDistances[FirstCity][SecondCity];

	int LastIndex = 0;
	int CurrentIndex = FirstCityIndex;
	while (true)
	{
		LastIndex = CurrentIndex;
		++CurrentIndex;
		if (CurrentIndex == InputData.CitiesCount)
		{
			CurrentIndex = 0;
		}
		if (CurrentIndex == SecondCityIndex)
		{
			break;
		}
		InputIndividual.Chromosome[LastIndex] = InputIndividual.Chromosome[CurrentIndex];
	}
	InputIndividual.Chromosome[LastIndex] = FirstCity;
	InputIndividual.FitnessValue += FitnessDelta;
}

void PerformIndividualsSorting(SalesmanIndividual** IndividualsArray, 
	int StartIndex, int EndIndex)
{
	if (EndIndex - StartIndex < 1) return; 
	int PivotIndex = PerformDivision(IndividualsArray, StartIndex, EndIndex); 
	PerformIndividualsSorting(IndividualsArray, StartIndex, PivotIndex - 1); 
	PerformIndividualsSorting(IndividualsArray, PivotIndex + 1, EndIndex); 
}

int PerformDivision(SalesmanIndividual** IndividualsArray, int StartIndex, int EndIndex)
{
	int PivotIndex = StartIndex;
	int PivotValue = IndividualsArray[EndIndex]->FitnessValue; 
	for (int CurrentIndex = StartIndex; CurrentIndex < EndIndex; CurrentIndex++)
	{
		if (IndividualsArray[CurrentIndex]->FitnessValue < PivotValue)
		{
			SwapItems(IndividualsArray, CurrentIndex, PivotIndex);
			PivotIndex++;
		}
	}
	SwapItems(IndividualsArray, PivotIndex, EndIndex);
	return PivotIndex;
}

void PerformPopulationsMerge(
	SalesmanIndividual** NewPopulation, 
	SalesmanIndividual* const* OldPopulation,
	int SurvivePortionLength,
	int PopulationSize,
	SalesmanIndividual** OutputPopulation)
{
	int OutputIndex = 0;
	int NewIndex = 0;
	int OldIndex = 0;
	int NewIndexLimit = PopulationSize - SurvivePortionLength;
	while ((NewIndex < NewIndexLimit) && (OldIndex < SurvivePortionLength))
	{
		if (NewPopulation[NewIndex]->FitnessValue > 
			OldPopulation[OldIndex]->FitnessValue)
		{
			OutputPopulation[OutputIndex++] = OldPopulation[OldIndex++];
		}
		else
		{
			OutputPopulation[OutputIndex++] = NewPopulation[NewIndex++];
		}
	}
	while (NewIndex < NewIndexLimit)
	{
		OutputPopulation[OutputIndex++] = NewPopulation[NewIndex++];
	}
	while (OldIndex < SurvivePortionLength)
	{
		OutputPopulation[OutputIndex++] = OldPopulation[OldIndex++];
	}
	//Aby unikn�� problem�w z pami�ci� porzucone osobniki ze starej
	//populacji powinny zosta� przekopiowane do tej, w kt�rej by�a
	//przechowywana nowa. B�dzie je mo�na w�wczas ponownie wykorzysta�
	//a tak�e usun��.
	OutputIndex = 0;
	OldIndex = SurvivePortionLength;
	while (OldIndex < PopulationSize)
	{
		NewPopulation[OutputIndex++] = OldPopulation[OldIndex++];
	}
}

SalesmanIndividual::SalesmanIndividual(int ChromosomeSize)
{
	FitnessValue = -1;
	Chromosome = new int[ChromosomeSize]();
}

void SalesmanIndividual::CopyFrom(const SalesmanIndividual& Base, 
	int ChromosomeSize)
{
	FitnessValue = Base.FitnessValue;
	std::copy(Base.Chromosome, Base.Chromosome + ChromosomeSize, 
		Chromosome);
}

void SalesmanIndividual::PutNearestNeighbourSolution(
	const SalesmanCities& InputData, int StartCity)
{
	FitnessValue = GetRouteArrayUsingNNA(
		InputData, StartCity, Chromosome);
}

void SalesmanIndividual::Randomize(const SalesmanCities& InputData)
{
	int SelectedCity = 0;
	bool* CityTakenStates = new bool[InputData.CitiesCount]();
 	std::uniform_int_distribution<int> CityDist(0, InputData.CitiesCount - 1);
	SelectedCity = CityDist(*RandomGenerator);
	Chromosome[0] = SelectedCity;
	CityTakenStates[SelectedCity] = true;
	int StartCity = SelectedCity;
	int LastCity = SelectedCity;
	FitnessValue = 0;
	for (int CityIndex = 1; CityIndex < InputData.CitiesCount; ++CityIndex)
	{
		do
		{
			SelectedCity = CityDist(*RandomGenerator);
		}
		while (CityTakenStates[SelectedCity]);
		FitnessValue += InputData.CitiesDistances[LastCity][SelectedCity];
		Chromosome[CityIndex] = SelectedCity;
		CityTakenStates[SelectedCity] = true;
		LastCity = SelectedCity;
	}
	FitnessValue += InputData.CitiesDistances[SelectedCity][StartCity];
	delete[] CityTakenStates;
}

void SalesmanIndividual::UpdateFitness(const SalesmanCities& InputData)
{
	int CurrentCity = 0;
	int LastCity = Chromosome[0];
	FitnessValue = 0;
	for (int CityIndex = 1; CityIndex < InputData.CitiesCount; ++CityIndex)
	{
		CurrentCity = Chromosome[CityIndex];
		FitnessValue += InputData.CitiesDistances[LastCity][CurrentCity];
		LastCity = CurrentCity;
	}
	FitnessValue += InputData.CitiesDistances[CurrentCity][Chromosome[0]];
}

SalesmanIndividual::~SalesmanIndividual()
{
	delete[] Chromosome;
}