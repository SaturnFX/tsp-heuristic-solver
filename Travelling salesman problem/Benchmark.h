#pragma once
#include "Global.h"

/*
* Modu� Benchmark
*
* Umo�liwia testowanie jako�ci rozwi�za�
* generowanych przez heurystyczne algorytmy 
* rozwi�zuj�ce problem komiwoja�era
*/

//Przeprowadza seri� test�w efektywno�ci r�nych
//sposob�w otrzymywania przybli�onych rozwi�za�
//aTSP i zapisuje wyniki do plik�w.
//UWAGA! Testy mog� trwa� bardzo d�ugo
void BenchmarkSalesman();