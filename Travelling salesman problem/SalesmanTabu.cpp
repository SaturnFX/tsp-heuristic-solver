#include "Global.h"
#include "SalesmanTabu.h"
#include "SalesmanCommon.h"
#include "Utilities.h"

#include <cmath>
#include <limits>

//Maksymalna mo�liwa ilo�� ruch�w zakazanych
//pami�tana podczas dzia�ania tabu-search
#define TS_TABU_MOVES_MAXIMUM 7

//Struktura opisuj�ca pojedy�czy zakaz
//u�ywany przez zaimplementowany tu
//algorytm. Przechowywana jest ona
//w 2-wymiarowej tablicy, kt�rej dwa indeksy 
//identyfikuj� 2 sk�adowe danego ruchu. 
//Dodatkowo, aby umo�liwi� usuwania nadmiarowych
//zakaz�w, aktywne z nich przechowuj�
//informacje o poprzednikach i nast�pnikach
//przez co ca�a struktura dzia�a tak�e 
//jak lista wi�zana
struct SalesmanTabuMove
{
	int Expiration;
	SalesmanTabuMove* PreviousMove;
	SalesmanTabuMove* NextMove;

	//Domy�lny konstruktor 
	//tworz�cy nieaktywny zakaz
	SalesmanTabuMove();
};

int SalesmanTabuSearchSwapHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, LinkedList<int>& OutputRoute)
{
	int FirstIndex = 0;
	int SecondIndex = 0;
	int FirstCity = 0;
	int FirstPrevCity = 0;
	int FirstNextCity = 0;
	int SecondCity = 0;
	int SecondPrevCity = 0;
	int SecondNextCity = 0;
	bool IsFirstBeforeSecond = false;
	bool IsSecondBeforeFirst = false;
	int CandidateFirstIndex = 0;
	int CandidateSecondIndex = 0;
	int MaximumCityIndex = InputData.CitiesCount - 1;

	//R�nice jako�ci badanych rozwi�za�
	int CurrentDelta = 0;
	int CandidateDelta = 0;
	//Por�wnanie jako�ci rozwi�zania najlepszego
	//oraz ostaniego, potrzebne do kryterium aspiracji.
	//Zawsze r�wne: MinimumDistance - LastDistance
	int MinimumToLastDelta = 0;

	//Wygenerowanie rozwi�zania z u�yciem algorytmu
	//najbli�szego s�siada, p�zniej tak�e wykorzystywany
	//przy dywersyfikacji przestrzeni przeszukiwania
	int StartCity = 0;
	int* LastRoute = new int[InputData.CitiesCount];
	int LastDistance = GetRouteArrayUsingNNA(
		InputData, StartCity, LastRoute);
	int* MinimumRoute = new int[InputData.CitiesCount];
	std::copy(LastRoute, LastRoute + InputData.CitiesCount, 
		MinimumRoute);
	int MinimumDistance = LastDistance;

	int PresentMovesCount = 0;
	bool AllowCurrentMove = false;
	SalesmanTabuMove* CurrentMove = nullptr;
	SalesmanTabuMove* FirstMove = nullptr;
	SalesmanTabuMove* LastMove = nullptr;
	//Deklaracja dwuwymiarowej tablicy zakaz�w
	SalesmanTabuMove** TabuMoves = new SalesmanTabuMove*[InputData.CitiesCount];
	//Kadencja danego ruchu w li�cie tabu r�wna pierwiastkowi
	//z ilo�ci wierzcho�k�w w grafie wej�ciowym
	int TabuExpiration = ((int) sqrt((double) InputData.CitiesCount));
	//Poniewa� w wypadku s�dzedztwa typu swap ruchy (i, j) i (j, i)
	//s� to�same, by zaoszcz�dzi� czas pami�tane s� tylko te
	//niepowtarzaj�ce si�.
	for (FirstCity = 1; FirstCity <= MaximumCityIndex; ++FirstCity)
	{
		TabuMoves[FirstCity] = new SalesmanTabuMove[FirstCity]();
	}

	//Zdefiniowanie moment�w czasu steruj�cych przebiegiem
	//algorytmu. Po up�yni�ciu ka�dej 1/n-tej czasu przeprowadzana
	//jest dywersyfikacja polegaj�ca na prze��czeniu obszaru
	//poszukiwania nowych rozwi�za� na kolejn�
	//z n wyznaczonych podprzestrzeni
	int64_t CurrentTime = GetUNIXMiliseconds();
	int64_t TotalMiliseconds = SecondsLimit * 1000;
	int64_t StageMiliseconds = TotalMiliseconds / InputData.CitiesCount;
	int64_t NextStageTime = CurrentTime + StageMiliseconds;
	int64_t FinishTime = CurrentTime + TotalMiliseconds;
	do
	{
		CandidateDelta = std::numeric_limits<int>::max();
		for (FirstIndex = 1; FirstIndex <= MaximumCityIndex; ++FirstIndex)
		{
			FirstCity = LastRoute[FirstIndex];
			if (FirstIndex == 0)
			{
				FirstPrevCity = LastRoute[InputData.CitiesCount - 1];
			}
			else
			{
				FirstPrevCity = LastRoute[FirstIndex - 1];
			}
			if (FirstIndex == MaximumCityIndex)
			{
				FirstNextCity = LastRoute[0];
			}
			else
			{
				FirstNextCity = LastRoute[FirstIndex + 1];
			}
			for (SecondIndex = 0; SecondIndex < FirstIndex; ++SecondIndex)
			{
				SecondCity = LastRoute[SecondIndex];
				if (SecondIndex == 0)
				{
					SecondPrevCity = LastRoute[InputData.CitiesCount - 1];
				}
				else
				{
					SecondPrevCity = LastRoute[SecondIndex - 1];
				}
				if (SecondIndex == MaximumCityIndex)
				{
					SecondNextCity = LastRoute[0];
				}
				else
				{
					SecondNextCity = LastRoute[SecondIndex + 1];
				}
				IsFirstBeforeSecond = (FirstNextCity == SecondCity);
				IsSecondBeforeFirst = (SecondNextCity == FirstCity);

				CurrentDelta = -InputData.CitiesDistances[FirstCity][FirstNextCity]
					- InputData.CitiesDistances[SecondCity][SecondNextCity];
				if (IsFirstBeforeSecond)
				{
					CurrentDelta += InputData.CitiesDistances[SecondCity][FirstCity];
				}
				else
				{
					CurrentDelta += (-InputData.CitiesDistances[SecondPrevCity][SecondCity]
						+ InputData.CitiesDistances[SecondCity][FirstNextCity]
						+ InputData.CitiesDistances[SecondPrevCity][FirstCity]);
				}
				if (IsSecondBeforeFirst)
				{
					CurrentDelta += InputData.CitiesDistances[FirstCity][SecondCity];
				}
				else
				{
					CurrentDelta += (-InputData.CitiesDistances[FirstPrevCity][FirstCity]
						+ InputData.CitiesDistances[FirstPrevCity][SecondCity]
						+ InputData.CitiesDistances[FirstCity][SecondNextCity]);
				}

				//Sprawdzenie, czy wybrany ruch jest
				//zakazany, czy te� nie
				AllowCurrentMove = true;
				if (FirstCity > SecondCity)
				{
					CurrentMove = &TabuMoves[FirstCity][SecondCity];
				}
				else
				{
					CurrentMove = &TabuMoves[SecondCity][FirstCity];
				}
				
				if (CurrentMove->Expiration > 0)
				{
					if (CurrentDelta >= MinimumToLastDelta)
					{
						AllowCurrentMove = false;
					}
					else
					{
						//Zastosowanie kryterium aspiracji
						//polegaj�cego na zaakceptowaniu trasy
						//je�eli jest ona najlepsza ze wszystkich
						//dot�d znalezionych
						CurrentMove->Expiration = 1;
					}
					if ((--CurrentMove->Expiration) == 0)
					{
						//Usuni�cie zakazu, kt�ry straci�
						//swoj� wa�no�c
						if (CurrentMove->PreviousMove)
						{
							CurrentMove->PreviousMove->NextMove = CurrentMove->NextMove;
						}
						else
						{
							FirstMove = CurrentMove->NextMove;
						}
						if (CurrentMove->NextMove)
						{
							CurrentMove->NextMove->PreviousMove = CurrentMove->PreviousMove;
						}
						else
						{
							LastMove = CurrentMove->PreviousMove;
						}

						--PresentMovesCount;
					}
				}

				if (AllowCurrentMove && (CurrentDelta < CandidateDelta))
				{
					CandidateDelta = CurrentDelta;
					CandidateFirstIndex = FirstIndex;
					CandidateSecondIndex = SecondIndex;
				}
			}
		}
		if (CandidateDelta == std::numeric_limits<int>::max())
		{
			throw AlgorithmException("Could not found any feasible move");
		}
		//Zastosowanie wybranej transformacji
		FirstCity = LastRoute[CandidateFirstIndex];
		SecondCity = LastRoute[CandidateSecondIndex];
		LastRoute[CandidateSecondIndex] = FirstCity;
		LastRoute[CandidateFirstIndex] = SecondCity;
		LastDistance += CandidateDelta;
		MinimumToLastDelta -= CandidateDelta;
		if (LastDistance < MinimumDistance)
		{
			std::copy(LastRoute, LastRoute + InputData.CitiesCount, 
				MinimumRoute);
			MinimumDistance = LastDistance;
			MinimumToLastDelta = 0;
		}

		//Dodanie ruchu odwrotnego od aktualnie wybranego
		//do struktury przechowuj�cej zakazy - tzw. lista tabu.
		//W wypadku tego rodzaju s�siedztwa odwrotno�ci�
		//transformacji (i, j) jest r�wnie� transformacja
		//(i, j)
		if (FirstCity > SecondCity)
		{
			CurrentMove = &TabuMoves[FirstCity][SecondCity];
		}
		else
		{
			CurrentMove = &TabuMoves[SecondCity][FirstCity];
		}
		CurrentMove->Expiration = TabuExpiration;
		CurrentMove->PreviousMove = nullptr;
		if (FirstMove)
		{
			FirstMove->PreviousMove = CurrentMove;
		}
		else
		{
			LastMove = CurrentMove;
		}
		CurrentMove->NextMove = FirstMove;
		FirstMove = CurrentMove;
		//Sprawdzenie, czy lista zakaz�w nie zosta�a
		//przepe�niona
		if ((++PresentMovesCount) > TS_TABU_MOVES_MAXIMUM)
		{
			//Usuni�cie ostatniego ruchu z listy
			LastMove->Expiration = 0;
			//Warto�� pola PreviousMove dotychczasowo
			//ostaniego zakazu zostaje zignorowana
			//gdy� nie wp�ywa na przebieg algorytmu
			LastMove = LastMove->PreviousMove;
			LastMove->NextMove = nullptr;
			--PresentMovesCount;
		}
		//Przeprowadzenie dywersyfikacji
		//przestrzeni przeszukiwanej przez
		//algorytm
		CurrentTime = GetUNIXMiliseconds();
		if (CurrentTime >= NextStageTime)
		{
			++StartCity;
			//Dodatkowy warunek zako�czenia: 
			//je�li wszystkie podprzestrzenie
			//wygenerowane algorytmem NNA zosta�y
			//ju� przeszukane nie ma sensu kontynuowa�
			//i p�tla jest przerywana
			if (StartCity > MaximumCityIndex)
			{
				break;
			}
			NextStageTime = NextStageTime + StageMiliseconds;
			LastDistance = GetRouteArrayUsingNNA(
				InputData, StartCity, LastRoute);
			MinimumToLastDelta = MinimumDistance - LastDistance;
			if (LastDistance < MinimumDistance)
			{
				std::copy(LastRoute, LastRoute + InputData.CitiesCount, 
					MinimumRoute);
				MinimumDistance = LastDistance;
				MinimumToLastDelta = 0;
			}
		}
	} 
	//G��wny warunek zako�czenia algorytmu, przyjmuj�cy 
	//warto�� logiczn� "prawda" po up�yni�ciu
	//danego okresu czasu
	while (CurrentTime < FinishTime);
	for (FirstCity = 1; FirstCity <= MaximumCityIndex; ++FirstCity)
	{
		delete[] TabuMoves[FirstCity];
	}
	delete[] TabuMoves;
	delete[] LastRoute;
	OutputRoute.Load(MinimumRoute, InputData.CitiesCount);
	OutputRoute.AddLast(MinimumRoute[0]);
	delete[] MinimumRoute;
	return MinimumDistance;
}

int SalesmanTabuSearchInsertHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, LinkedList<int>& OutputRoute)
{
	int FirstCity = 0;
	int FirstPrevCity = 0;
	int FirstNextCity = 0;
	int SecondCity = 0;
	int SecondPrevCity = 0;
	int CandidateFirstCity = 0;
	int CandidateSecondCity = 0;
	int MaximumCityIndex = InputData.CitiesCount - 1;

	int CurrentDelta = 0;
	int CandidateDelta = 0;
	int MinimumToLastDelta = 0;

	int StartCity = 0;
	int* LastForwardConnections = new int[InputData.CitiesCount];
	int* LastBackwardConnections = new int[InputData.CitiesCount];
	int LastDistance = GetRouteTwoWayConnectionsUsingNNA(
		InputData, StartCity, LastForwardConnections, 
		LastBackwardConnections);
	int* MinimumConnections = new int[InputData.CitiesCount];
	std::copy(LastForwardConnections, 
		LastForwardConnections + InputData.CitiesCount, 
		MinimumConnections);
	int MinimumDistance = LastDistance;

	int PresentMovesCount = 0;
	bool AllowCurrentMove = false;
	SalesmanTabuMove* CurrentMove = nullptr;
	SalesmanTabuMove* FirstMove = nullptr;
	SalesmanTabuMove* LastMove = nullptr;
	SalesmanTabuMove** TabuMoves = new SalesmanTabuMove*[InputData.CitiesCount];
	int TabuExpiration = ((int) sqrt((double) InputData.CitiesCount));
	for (FirstCity = 0; FirstCity <= MaximumCityIndex; ++FirstCity)
	{
		TabuMoves[FirstCity] = new SalesmanTabuMove[InputData.CitiesCount]();
	}

	int64_t CurrentTime = GetUNIXMiliseconds();
	int64_t TotalMiliseconds = SecondsLimit * 1000;
	int64_t StageMiliseconds = TotalMiliseconds / InputData.CitiesCount;
	int64_t NextStageTime = CurrentTime + StageMiliseconds;
	int64_t FinishTime = CurrentTime + TotalMiliseconds;
	do
	{
		CandidateDelta = std::numeric_limits<int>::max();
		//S�siedztwo typu Insert polega na wybraniu
		//dw�ch miast - pary (i, j), z kt�rej pierwsze jest 
		//wstawiane na miejsce drugiego
		for (FirstCity = 0; FirstCity <= MaximumCityIndex; ++FirstCity)
		{
			FirstNextCity = LastForwardConnections[FirstCity];
			FirstPrevCity = LastBackwardConnections[FirstCity];
			for (SecondCity = 0; SecondCity <= MaximumCityIndex; ++SecondCity)
			{
				if (SecondCity == FirstCity) continue;
				//W s�siedztwie tym grupa transformacji
				//(i, i + 1) wybierana by�aby dwukrotnie, dlatego blokowana
				//jest dok�adnie po�owa takich wyst�pie� - tych, w kt�rych
				//drugie wybrane miasto stoi tu� za pierwszym
				AllowCurrentMove = (SecondCity != FirstNextCity);

				if (AllowCurrentMove)
				{
					SecondPrevCity = LastBackwardConnections[SecondCity];

					//Obliczenie r�nicy d�ugo�ci ostatniej trasy oraz tej
					//powstaj�cej z wybranej transformacji poprzez odj�cie
					//d�ugo�ci usuwanych po��cze�, a dodanie nowo utworzonych 
					CurrentDelta = -InputData.CitiesDistances[FirstPrevCity][FirstCity]
						- InputData.CitiesDistances[FirstCity][FirstNextCity]
						- InputData.CitiesDistances[SecondPrevCity][SecondCity]
						+ InputData.CitiesDistances[FirstPrevCity][FirstNextCity]
						+ InputData.CitiesDistances[SecondPrevCity][FirstCity]
						+ InputData.CitiesDistances[FirstCity][SecondCity];
				}

				CurrentMove = &TabuMoves[FirstCity][SecondCity];
				if (CurrentMove->Expiration > 0)
				{
					if (CurrentDelta >= MinimumToLastDelta)
					{
						AllowCurrentMove = false;
					}
					else
					{
						CurrentMove->Expiration = 1;
					}
					if ((--CurrentMove->Expiration) == 0)
					{
						if (CurrentMove->PreviousMove)
						{
							CurrentMove->PreviousMove->NextMove = CurrentMove->NextMove;
						}
						else
						{
							FirstMove = CurrentMove->NextMove;
						}
						if (CurrentMove->NextMove)
						{
							CurrentMove->NextMove->PreviousMove = CurrentMove->PreviousMove;
						}
						else
						{
							LastMove = CurrentMove->PreviousMove;
						}

						--PresentMovesCount;
					}
				}

				if (AllowCurrentMove && (CurrentDelta < CandidateDelta))
				{
					CandidateDelta = CurrentDelta;
					CandidateFirstCity = FirstCity;
					CandidateSecondCity = SecondCity;
				}
			}
		}
		if (CandidateDelta == std::numeric_limits<int>::max())
		{
			throw AlgorithmException("Could not found any feasible move");
		}

		FirstNextCity = LastForwardConnections[CandidateFirstCity];
		FirstPrevCity = LastBackwardConnections[CandidateFirstCity];
		SecondPrevCity = LastBackwardConnections[CandidateSecondCity];

		LastForwardConnections[SecondPrevCity] = CandidateFirstCity;
		LastBackwardConnections[CandidateFirstCity] = SecondPrevCity;
		LastForwardConnections[CandidateFirstCity] = CandidateSecondCity;
		LastBackwardConnections[CandidateSecondCity] = CandidateFirstCity;
		LastForwardConnections[FirstPrevCity] = FirstNextCity;
		LastBackwardConnections[FirstNextCity] = FirstPrevCity;
		LastDistance += CandidateDelta;
		MinimumToLastDelta -= CandidateDelta;
		if (LastDistance < MinimumDistance)
		{
			std::copy(LastForwardConnections, 
				LastForwardConnections + InputData.CitiesCount, 
				MinimumConnections);
			MinimumDistance = LastDistance;
			MinimumToLastDelta = 0;
		}
		
		//Ruchem odwrotnym do (i, j) jest ruch (i, i_n) gdzie
		//i_n jest nast�pnym miastem po i przed dokonaniem
		//transformacji
		CurrentMove = &TabuMoves[CandidateFirstCity][FirstNextCity];
		if (CurrentMove->Expiration == 0) 
		{
			CurrentMove->Expiration = TabuExpiration;
			CurrentMove->PreviousMove = nullptr;
			if (FirstMove)
			{
				FirstMove->PreviousMove = CurrentMove;
			}
			else
			{
				LastMove = CurrentMove;
			}
			CurrentMove->NextMove = FirstMove;
			FirstMove = CurrentMove;
			if ((++PresentMovesCount) > TS_TABU_MOVES_MAXIMUM)
			{
				LastMove->Expiration = 0;
				LastMove = LastMove->PreviousMove;
				LastMove->NextMove = nullptr;
				--PresentMovesCount;
			}
		}
		else
		{
			CurrentMove->Expiration = TabuExpiration;
		}
		CurrentTime = GetUNIXMiliseconds();
		if (CurrentTime >= NextStageTime)
		{
			++StartCity;
			if (StartCity > MaximumCityIndex)
			{
				break;
			}
			NextStageTime = NextStageTime + StageMiliseconds;
			LastDistance = GetRouteTwoWayConnectionsUsingNNA(
				InputData, StartCity, LastForwardConnections, 
				LastBackwardConnections);
			MinimumToLastDelta = MinimumDistance - LastDistance;
			if (LastDistance < MinimumDistance)
			{
				std::copy(LastForwardConnections, 
					LastForwardConnections + InputData.CitiesCount, 
					MinimumConnections);
				MinimumDistance = LastDistance;
				MinimumToLastDelta = 0;
			}
		}
	} 
	while (CurrentTime < FinishTime);
	for (FirstCity = 1; FirstCity <= MaximumCityIndex; ++FirstCity)
	{
		delete[] TabuMoves[FirstCity];
	}
	delete[] TabuMoves;
	delete[] LastBackwardConnections;
	delete[] LastForwardConnections;
	ConvertRouteConnectionsToList(MinimumConnections, OutputRoute);
	delete[] MinimumConnections;
	return MinimumDistance;
}

int SalesmanTabuSearchInvertHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, LinkedList<int>& OutputRoute)
{
	int FirstIndex = 0;
	int SecondIndex = 0;
	int FirstCity = 0;
	int SecondCity = 0;

	int InvertIndex = 0;
	int InvertPrevCity = 0;
	int InvertCity = 0;

	int CandidateFirstIndex = 0;
	int CandidateSecondIndex = 0;
	int MaximumCityIndex = InputData.CitiesCount - 1;

	int CurrentDelta = 0;
	int CandidateDelta = 0;
	int MinimumToLastDelta = 0;

	int SwapLastIndex = 0;
	int ForwardSwapIndex = 0;
	int BackwardSwapIndex = 0;

	int StartCity = 0;
	int* LastRoute = new int[InputData.CitiesCount];
	int LastDistance = GetRouteArrayUsingNNA(
		InputData, StartCity, LastRoute);
	int* MinimumRoute = new int[InputData.CitiesCount];
	std::copy(LastRoute, LastRoute + InputData.CitiesCount, 
		MinimumRoute);
	int MinimumDistance = LastDistance;

	int PresentMovesCount = 0;
	bool AllowCurrentMove = false;
	SalesmanTabuMove* CurrentMove = nullptr;
	SalesmanTabuMove* FirstMove = nullptr;
	SalesmanTabuMove* LastMove = nullptr;
	SalesmanTabuMove** TabuMoves = new SalesmanTabuMove*[InputData.CitiesCount];
	int TabuExpiration = ((int) sqrt((double) InputData.CitiesCount));
	for (FirstCity = 0; FirstCity <= MaximumCityIndex; ++FirstCity)
	{
		TabuMoves[FirstCity] = new SalesmanTabuMove[InputData.CitiesCount]();
	}

	int64_t CurrentTime = GetUNIXMiliseconds();
	int64_t TotalMiliseconds = SecondsLimit * 1000;
	int64_t StageMiliseconds = TotalMiliseconds / InputData.CitiesCount;
	int64_t NextStageTime = CurrentTime + StageMiliseconds;
	int64_t FinishTime = CurrentTime + TotalMiliseconds;
	do
	{
		CandidateDelta = std::numeric_limits<int>::max();
		//Po s�siedztwe Invert algorytm porusza si� za pomoc�
		//transformacji, kt�ra dla danej pary miast (i, j)
		//odwracana jest kolejno�� po��cze� pomi�dzy nimi
		//rozpoczynaj�c od miasta i
		for (FirstIndex = 0; FirstIndex <= MaximumCityIndex; ++FirstIndex)
		{
			FirstCity = LastRoute[FirstIndex];
			for (SecondIndex = 0; SecondIndex <= MaximumCityIndex; ++SecondIndex)
			{
				if (SecondIndex == FirstIndex) continue;
				SecondCity = LastRoute[SecondIndex];

				//Obliczenie r�nicy d�ugo�ci ostatniej trasy 
				//i aktualnie badanej poprzez odj�cie d�ugo�ci 
				//zmienianych i dodanie nowych d�ugo�ci
				//- po��cze� biegn�cych w przeciwnym kierunku
				//na danym odcinku
				if (FirstIndex == 0) InvertPrevCity = LastRoute[MaximumCityIndex];
				else InvertPrevCity = LastRoute[FirstIndex - 1];

				CurrentDelta = -InputData.CitiesDistances[InvertPrevCity][FirstCity]
					+ InputData.CitiesDistances[InvertPrevCity][SecondCity];

				InvertPrevCity = FirstCity;
				if (FirstIndex != MaximumCityIndex) InvertIndex = FirstIndex + 1;
				else InvertIndex = 0;
				InvertCity = LastRoute[InvertIndex];

				while (InvertPrevCity != SecondCity)
				{
					CurrentDelta += (
						-InputData.CitiesDistances[InvertPrevCity][InvertCity]
						+ InputData.CitiesDistances[InvertCity][InvertPrevCity]);
					
					InvertPrevCity = InvertCity;
					if (InvertIndex != MaximumCityIndex) ++InvertIndex;
					else InvertIndex = 0;
					InvertCity = LastRoute[InvertIndex];
				}

				CurrentDelta += (-InputData.CitiesDistances[SecondCity][InvertCity]
					+ InputData.CitiesDistances[FirstCity][InvertCity]);

				AllowCurrentMove = true;
				CurrentMove = &TabuMoves[FirstCity][SecondCity];
				
				if (CurrentMove->Expiration > 0)
				{
					if (CurrentDelta >= MinimumToLastDelta)
					{
						AllowCurrentMove = false;
					}
					else
					{
						CurrentMove->Expiration = 1;
					}
					if ((--CurrentMove->Expiration) == 0)
					{
						if (CurrentMove->PreviousMove)
						{
							CurrentMove->PreviousMove->NextMove = CurrentMove->NextMove;
						}
						else
						{
							FirstMove = CurrentMove->NextMove;
						}
						if (CurrentMove->NextMove)
						{
							CurrentMove->NextMove->PreviousMove = CurrentMove->PreviousMove;
						}
						else
						{
							LastMove = CurrentMove->PreviousMove;
						}

						--PresentMovesCount;
					}
				}

				if (AllowCurrentMove && (CurrentDelta < CandidateDelta))
				{
					CandidateDelta = CurrentDelta;
					CandidateFirstIndex = FirstIndex;
					CandidateSecondIndex = SecondIndex;
				}
			}
		}
		if (CandidateDelta == std::numeric_limits<int>::max())
		{
			throw AlgorithmException("Could not found any feasible move");
		}

		FirstCity = LastRoute[CandidateFirstIndex];
		SecondCity = LastRoute[CandidateSecondIndex];

		SwapLastIndex = -1;
		ForwardSwapIndex = CandidateFirstIndex;
		BackwardSwapIndex = CandidateSecondIndex;
		while (!((ForwardSwapIndex == BackwardSwapIndex) || 
			(SwapLastIndex == BackwardSwapIndex)))
		{
			SwapItems<int>(LastRoute, ForwardSwapIndex, BackwardSwapIndex);
			SwapLastIndex = ForwardSwapIndex;
			if (ForwardSwapIndex != MaximumCityIndex) ++ForwardSwapIndex;
			else ForwardSwapIndex = 0;
			if (BackwardSwapIndex != 0) --BackwardSwapIndex;
			else BackwardSwapIndex = MaximumCityIndex;
		}
		LastDistance += CandidateDelta;
		MinimumToLastDelta -= CandidateDelta;
		if (LastDistance < MinimumDistance)
		{
			std::copy(LastRoute, LastRoute + InputData.CitiesCount, 
				MinimumRoute);
			MinimumDistance = LastDistance;
			MinimumToLastDelta = 0;
		}

		//Ruchem odwrotnym do ruchu (i, j) dla s�siedztwa
		//typu Invert jest tak�e (i, j)
		CurrentMove = &TabuMoves[FirstCity][SecondCity];
		CurrentMove->Expiration = TabuExpiration;
		CurrentMove->PreviousMove = nullptr;
		if (FirstMove)
		{
			FirstMove->PreviousMove = CurrentMove;
		}
		else
		{
			LastMove = CurrentMove;
		}
		CurrentMove->NextMove = FirstMove;
		FirstMove = CurrentMove;
		if ((++PresentMovesCount) > TS_TABU_MOVES_MAXIMUM)
		{
			LastMove->Expiration = 0;
			LastMove = LastMove->PreviousMove;
			LastMove->NextMove = nullptr;
			--PresentMovesCount;
		}
		CurrentTime = GetUNIXMiliseconds();
		if (CurrentTime >= NextStageTime)
		{
			++StartCity;
			if (StartCity > MaximumCityIndex)
			{
				break;
			}
			NextStageTime = NextStageTime + StageMiliseconds;
			LastDistance = GetRouteArrayUsingNNA(
				InputData, StartCity, LastRoute);
			MinimumToLastDelta = MinimumDistance - LastDistance;
			if (LastDistance < MinimumDistance)
			{
				std::copy(LastRoute, LastRoute + InputData.CitiesCount, 
					MinimumRoute);
				MinimumDistance = LastDistance;
				MinimumToLastDelta = 0;
			}
		}
	}
	while (CurrentTime < FinishTime);
	for (FirstCity = 1; FirstCity <= MaximumCityIndex; ++FirstCity)
	{
		delete[] TabuMoves[FirstCity];
	}
	delete[] TabuMoves;
	delete[] LastRoute;
	OutputRoute.Load(MinimumRoute, InputData.CitiesCount);
	OutputRoute.AddLast(MinimumRoute[0]);
	delete[] MinimumRoute;
	return MinimumDistance;
}

SalesmanTabuMove::SalesmanTabuMove()
	:Expiration(0), PreviousMove(nullptr), NextMove(nullptr)
{}