#include "Global.h"
#include "Exception.h"
#include "SalesmanData.h"
#include "LinkedList.h"
#include "SalesmanIO.h"
#include "SalesmanHeldKarp.h"
#include "SalesmanNearest.h"
#include "SalesmanAnnealing.h"
#include "SalesmanTabu.h"
#include "SalesmanGenetic.h"
#include "Benchmark.h"
#include "Utilities.h"

#include <conio.h>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

//Definicja kodu klawisza ENTER
#define ENTER_KEY 0xD

//Domy�lne parametry zaimplementowanych
//algorytm�w aTSP
#define DEFAULT_SECONDS_LIMIT 30
#define DEFAULT_NNA_START_CITY 0
#define DEFAULT_SA_TEMPERATURE_STEP 0.999999
#define DEFAULT_TS_NEIGHBORHOOD_TYPE 0
#define DEFAULT_GA_POPULATION_SIZE 1000
#define DEFAULT_GA_CROSSING_RATE 0.8
#define DEFAULT_GA_MUTATION_RATE 0.1
#define DEFAULT_GA_CROSSING_METHOD 0
#define DEFAULT_GA_MUTATION_METHOD 0

//Definicje rodzaj�w dost�pnych s�siedztw
//dla algorytmu Tabu Search oraz 
//mutacji dla algorytmu genetycznego
#define TSP_TRANSFORM_SWAP 0
#define TSP_TRANSFORM_INSERT 1
#define TSP_TRANSFORM_INVERT 2

//Definicje metod krzy�owa� dla
//algorytmu genetycznego
#define GA_CROSSING_PMX 0
#define GA_CROSSING_OX 1

//Metoda wy�wietlaj�ca menu g��wne programu
void PrintProgramMenu();
//Metoda wy�wietlaj�ca menu algorytm�w
void PrintAlgorithmsMenu();
//Funkcja wy�wietlaj�ca menu inicjalizacji danych
//wej�ciowych algorytm�w rozw. TSP
bool PrintCitiesInitializationMenu(SalesmanCities*& CitiesData);
//Metoda wy�wietlaj�ca rodzaj transformacji TSP
void PrintSalesmanTransformType(int TransformType);

//Metoda wypisuj�ca komunikat w oknie konsoli
void PrintMessage(std::string MessagePrompt);
//Funkcja pobieraj�ca �a�cuch znak�w z wej�cia konsoli
std::string FetchString(std::string MessagePrompt);
//Funkcja pobieraj�ca �a�cuch znak�w a� do
//klawisza ENTER z wej�cia konsoli
std::string FetchStringWithSpaces(std::string MessagePrompt);
//Metoda wypisuj�ca informacje o programie
void PrintProgramInformation();
//Metoda zatrzymuj�ca wykonanie i programu
//i czekaj�ca na potwierdzenie kontynuacji
void PressEnterKeyToContinue();
//Metoda czyszcz�ca konsol�
void ClearConsole();

//Funkcja startowa programu
int wmain(int argc, wchar_t *argv[], wchar_t *envp[])
{
	PrintProgramMenu();
	return 0;
}

void PrintProgramMenu()
{
	int FetchedCharacter = 0;
	while (FetchedCharacter != 'c')
	{
		ClearConsole();
		PrintProgramInformation();
		std::cout << "Co chcesz zrobi\x86 ?" << std::endl;
		std::cout << "(a) Przetestuj algorytmy rozwi\xA5zuj\xA5""ce aTSP" << std::endl;
		std::cout << "(b) Przeprowad\xAB testy jako\x98""ci rozwi\xA5za\xE4" << std::endl;
		std::cout << "(c) Wyjd\xAB z programu" << std::endl;
		std::cout << std::endl;
		std::cout << ":";
		FetchedCharacter = _getch();
		switch (FetchedCharacter)
		{
			case 'a':
				PrintAlgorithmsMenu();
				break;
			case 'b':
				ClearConsole();
				BenchmarkSalesman();
				PressEnterKeyToContinue();
		}
	}
}

void PrintAlgorithmsMenu()
{
	int RouteDistance = 0;
	LinkedList<int> OutputRoute;
	SalesmanCities* CitiesData = nullptr;
	int SecondsLimit = DEFAULT_SECONDS_LIMIT;
	int StartCity = DEFAULT_NNA_START_CITY;
	int NewStartCity = 0;
	double TemperatureStep = DEFAULT_SA_TEMPERATURE_STEP;
	int NeighborhoodType = DEFAULT_TS_NEIGHBORHOOD_TYPE;
	int PopulationSize = DEFAULT_GA_POPULATION_SIZE;
	double CrossingRate = DEFAULT_GA_CROSSING_RATE;
	double MutationRate = DEFAULT_GA_MUTATION_RATE;
	int CrossingMethod = DEFAULT_GA_CROSSING_METHOD;
	int MutationMethod = DEFAULT_GA_MUTATION_METHOD;
	int NewIDValue = 0;
	std::string ExceptionMessage;
	int FetchedCharacter = 0;
	if (!PrintCitiesInitializationMenu(CitiesData))
	{
		return;
	}
	while (FetchedCharacter != 's')
	{
		ClearConsole();
		PrintProgramInformation();
		std::cout << "Liczba miast: "
			<< CitiesData->CitiesCount << std::endl;
		std::cout << std::endl;
		std::cout << "Ustawione warto\x98""ci parametr\xA2w:" << std::endl;
		std::cout << "Limit czasu wykonania: " << SecondsLimit << " s" << std::endl;
		std::cout << "Miasto pocz\xA5tkowe (dla NS): " << StartCity << std::endl;
		std::cout << "Wsp\xA2\x88""czynnik spadku temperatury (dla SW): " << TemperatureStep << std::endl;
		std::cout << "Rozdzaj s\xA5siedztwa (dla PzZ): ";
		PrintSalesmanTransformType(NeighborhoodType);
		std::cout << std::endl;
		std::cout << "Wielko\x98\x86 populacji (dla AG): " << PopulationSize << std::endl;
		std::cout << "Wsp\xA2\x88""czynnik krzy\xBEowania (dla AG): " << CrossingRate << std::endl;
		std::cout << "Wsp\xA2\x88""czynnik mutacji (dla AG): " <<  MutationRate << std::endl;
		std::cout << "Metoda krzy\xBEowania (dla AG): ";
		switch (CrossingMethod)
		{
		case GA_CROSSING_PMX:
			std::cout << "PMX";
			break;
		case GA_CROSSING_OX:
			std::cout << "OX";
			break;
		}
		std::cout << std::endl;
		std::cout << "Metoda mutacji (dla AG): "; 
		PrintSalesmanTransformType(MutationMethod);
		std::cout << std::endl << std::endl;
		std::cout << "Co chcesz zrobi\x86 ?" << std::endl;
		std::cout << "(a) Wy\x98wietl dane wej\x98""ciowe" << std::endl;
		std::cout << "(b) Za\x88""aduj nowe dane" << std::endl;
		std::cout << "(c) Ustaw limit czasu wykonania" << std::endl;
		std::cout << "(d) Ustaw miasto pocz\xA5tkowe (dla NS)" << std::endl;
		std::cout << "(e) Ustaw wsp\xA2\x88""czynnik spadku temperatury (dla SW)" << std::endl;
		std::cout << "(f) Wybierz rozdzaj s\xA5siedztwa (dla PzZ)" << std::endl;
		std::cout << "(g) Ustaw wielko\x98\x86 populacji (dla AG)" << std::endl;
		std::cout << "(h) Ustaw wsp\xA2\x88""czynnik krzy\xBEowania (dla AG)" << std::endl;
		std::cout << "(i) Ustaw wsp\xA2\x88""czynnik mutacji (dla AG)" << std::endl;
		std::cout << "(j) Wybierz metod\xA9 krzy\xBEowania (dla AG)" << std::endl;
		std::cout << "(k) Wybierz metod\xA9 mutacji (dla AG)" << std::endl;
		std::cout << "(l) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu najbli\xBEszego s\xA5siada" << std::endl;
		std::cout << "(m) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu symulowanego wy\xBE""arzania" << std::endl;
		std::cout << "(n) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu przeszukiwania z zakazami" << std::endl;
		std::cout << "(o) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu genetycznego" << std::endl;
		std::cout << "(p) Rozwi\xA5\xBE aTSP za pomoc\xA5 algorytmu Helda-Karpa (algorytm dok\x88""adny)" << std::endl;
		std::cout << "(q) Skalibrowanie algorytmu symulowanego wy\xBE""arzania" << std::endl;
		std::cout << "(r) Zresetowanie kalibracji algorytmu symulowanego wy\xBE""arzania" << std::endl;
		std::cout << "(s) Porzu\x86 dane i wr\xA2\x86" << std::endl;
		std::cout << std::endl;
		std::cout << ":";
		FetchedCharacter = _getch();
		try
		{
			switch (FetchedCharacter)
			{
			case 'a':
				ClearConsole();
				PrintProgramInformation();
				PrintSalesmanCities(*CitiesData);
				PressEnterKeyToContinue();
				break;
			case 'b':
				if (PrintCitiesInitializationMenu(CitiesData))
				{
					StartCity = DEFAULT_NNA_START_CITY;
				}
				break;
			case 'c':
				SecondsLimit = std::stoi(FetchString("Podaj limit czasu wykonania (w sekundach)"));
				break;
			case 'd':
				NewStartCity = std::stoi(FetchString("Podaj miasto pocz\xA5tkowe"));
				if ((NewStartCity < 0) || ((NewStartCity >= CitiesData->CitiesCount)))
				{
					PrintMessage("Wprowadzono nieprawid\x88owe miasto pocz\xA5tkowe");
					break;
				}
				StartCity = NewStartCity;
				break;
			case 'e':
				TemperatureStep = std::stod(FetchString("Podaj wsp\xA2\x88""czynnik spadku temperatury"));
				break;
			case 'f':
				NewIDValue = std::stoi(FetchString("Wybierz rozdzaj s\xA5siedztwa (0 = swap; 1 = insert; 2 = invert)"));
				if ((NewIDValue < TSP_TRANSFORM_SWAP) || ((NewIDValue > TSP_TRANSFORM_INVERT)))
				{
					PrintMessage("Wprowadzono niedozwolony identyfikator s\xA5siedztwa");
					break;
				}
				NeighborhoodType = NewIDValue;
				break;
			case 'g':
				PopulationSize = std::stoi(FetchString("Podaj wielko\x98\x86 populacji"));
				break;
			case 'h':
				CrossingRate = std::stod(FetchString("Podaj wsp\xA2\x88""czynnik krzy\xBEowania"));
				break;
			case 'i':
				MutationRate = std::stod(FetchString("Podaj wsp\xA2\x88""czynnik mutacji"));
				break;
			case 'j':
				NewIDValue = std::stoi(FetchString("Wybierz metod\xA9 krzy\xBEowania (0 = PMX; 1 = OX)"));
				if ((NewIDValue < GA_CROSSING_PMX) || ((NewIDValue > GA_CROSSING_OX)))
				{
					PrintMessage("Wprowadzono niedozwolony identyfikator metody krzy\xBEowania");
					break;
				}
				CrossingMethod = NewIDValue;
				break;
			case 'k':
				NewIDValue = std::stoi(FetchString("Wybierz metod\xA9 mutacji (0 = swap; 1 = insert)"));
				if ((NewIDValue < TSP_TRANSFORM_SWAP) || ((NewIDValue > TSP_TRANSFORM_INSERT)))
				{
					PrintMessage("Wprowadzono niedozwolony identyfikator metody mutacji");
					break;
				}
				MutationMethod = NewIDValue;
				break;
			case 'l':
				ClearConsole();
				PrintProgramInformation();
				RouteDistance = SalesmanNearestHeurSolver(*CitiesData, StartCity, OutputRoute);
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
				break;
			case 'm':
				ClearConsole();
				PrintProgramInformation();
				RouteDistance = SalesmanAnnealingHeurSolver(*CitiesData, TemperatureStep, SecondsLimit, OutputRoute);
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
				break;
			case 'n':
				ClearConsole();
				PrintProgramInformation();
				if (NeighborhoodType == TSP_TRANSFORM_SWAP)
				{
					RouteDistance = SalesmanTabuSearchSwapHeurSolver(*CitiesData, SecondsLimit, OutputRoute);
				}
				else if (NeighborhoodType == TSP_TRANSFORM_INSERT)
				{
					RouteDistance = SalesmanTabuSearchInsertHeurSolver(*CitiesData, SecondsLimit, OutputRoute);
				}
				else if (NeighborhoodType == TSP_TRANSFORM_INVERT)
				{
					RouteDistance = SalesmanTabuSearchInvertHeurSolver(*CitiesData, SecondsLimit, OutputRoute);
				}
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
				break;
			case 'o':
				ClearConsole();
				PrintProgramInformation();
				if (MutationMethod == TSP_TRANSFORM_SWAP)
				{
					RouteDistance = SalesmanGeneticPMXSwapHeurSolver(*CitiesData, 
						SecondsLimit, PopulationSize, CrossingRate, MutationRate, OutputRoute);
				}
				else if (MutationMethod == TSP_TRANSFORM_INSERT)
				{
					RouteDistance = SalesmanGeneticPMXInsertHeurSolver(*CitiesData, 
						SecondsLimit, PopulationSize, CrossingRate, MutationRate, OutputRoute);
				}
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
				break;
			case 'p':
				ClearConsole();
				PrintProgramInformation();
				RouteDistance = SalesmanHeldKarpSolver(*CitiesData, OutputRoute);
				PrintSalesmanRoute(OutputRoute, RouteDistance);
				PressEnterKeyToContinue();
				OutputRoute.Clear();
				break;
			case 'q':
				ClearConsole();
				PrintProgramInformation();
				CalibrateSalesmanAnnealing(CitiesData->CitiesCount);
				std::cout << "Pomy\x98lnie skalibrowano algorytm symulowanego wy\xBE""arzania" << std::endl;
				PressEnterKeyToContinue();
				break;
			case 'r':
				ClearConsole();
				PrintProgramInformation();
				ResetSalesmanAnnealingCalibration();
				std::cout << "Pomy\x98lnie zresetowano kalibracj\xA9 algorytmu symulowanego wy\xBE""arzania" << std::endl;
				PressEnterKeyToContinue();
				break;
			}
		}
		catch (const std::exception& Exception)
		{
			ExceptionMessage = "Podczas operacji wyst\xA5pi\x88 nast\xA9puj\xA5""cy b\x88\xA5""d: " ;
			ExceptionMessage += Exception.what();
			PrintMessage(ExceptionMessage);
		}
	}
}

bool PrintCitiesInitializationMenu(SalesmanCities*& CitiesData)
{
	int CitiesNumber = 0;
	int DistanceMinimum = 0;
	int DistanceMaximum = 0;
	std::string FilePathString;
	SalesmanCities* NewCitiesData = nullptr;
	std::string ExceptionMessage;
	int FetchedCharacter = 0;
	while (FetchedCharacter != 'd')
	{
		ClearConsole();
		PrintProgramInformation();
		std::cout << "Sk\xA5""d za\x88""adowa\x86 graf miast komiwoja\xBE""era?" << std::endl;
		std::cout << "(a) Wygeneruj dane losowo" << std::endl;
		std::cout << "(b) Za\x88""aduj dane z pliku w formacie prostym (d\x88ugo\x98\x86 + macierz)" << std::endl;
		std::cout << "(c) Za\x88""aduj dane z pliku w formacie TSPLIB" << std::endl;
		std::cout << "(d) Wr\xA2\x86 do poprzedniego menu" << std::endl;
		std::cout << std::endl;
		std::cout << ":";
		FetchedCharacter = _getch();
		try
		{
			switch (FetchedCharacter)
			{
			case 'a':
				CitiesNumber = std::stoi(FetchString("Podaj liczb\xA9 miast"));
				DistanceMinimum = std::stoi(
					FetchString("Podaj dolne ograniczenie losowanych odleg\x88o\x98""ci mi\xA9""dzy miastami"));
				DistanceMaximum = std::stoi(
					FetchString("Podaj g\xA2rne ograniczenie losowanych odleg\x88o\x98""ci mi\xA9""dzy miastami"));
				NewCitiesData = new SalesmanCities(CitiesNumber);
				FillSalesmanCitiesWithRandomDistances(*NewCitiesData, DistanceMinimum, DistanceMaximum);
				if (CitiesData != nullptr) delete CitiesData;
				CitiesData = NewCitiesData;
				return true;
			case 'b':
				FilePathString = FetchStringWithSpaces("Podaj \x98""cie\xBEk\xA9 do pliku");
				if (LoadSalesmanCitiesFromFile(FilePathString, NewCitiesData) < 0)
				{
					PrintMessage(std::string("Wyst\xA5pi\x88 b\x88\xA5""d"
						" podczas oczytywania zawarto\x98""ci pliku"));
					return false;
				}
				if (CitiesData != nullptr) delete CitiesData;
				CitiesData = NewCitiesData;
				return true;
			case 'c':
				FilePathString = FetchStringWithSpaces("Podaj \x98""cie\xBEk\xA9 do pliku");
				if (LoadSalesmanCitiesFromTSPLIBFile(FilePathString, NewCitiesData) < 0)
				{
					PrintMessage(std::string("Wyst\xA5pi\x88 b\x88\xA5""d"
						" podczas oczytywania zawarto\x98""ci pliku"));
					return false;
				}
				if (CitiesData != nullptr) delete CitiesData;
				CitiesData = NewCitiesData;
				return true;
			}
		}
		catch (const std::exception& Exception)
		{
			ExceptionMessage = "Podczas \x88""adowania danych wyst\xA5pi\x88 nast\xA9puj\xA5""cy b\x88\xA5""d: " ;
			ExceptionMessage += Exception.what();
			PrintMessage(ExceptionMessage);
			break;
		}
	}
	return false;
}

void PrintSalesmanTransformType(int TransformType)
{
	switch (TransformType)
	{
	case TSP_TRANSFORM_SWAP:
		std::cout << "swap (zamiana)";
		break;
	case TSP_TRANSFORM_INSERT:
		std::cout << "insert (wstawienie)";
		break;
	case TSP_TRANSFORM_INVERT:
		std::cout << "invert (odwr\xA2""cenie kolejno\x98""ci)";
	}
}

void PrintMessage(std::string MessagePrompt)
{
	ClearConsole();
	PrintProgramInformation();
	std::cout << MessagePrompt << std::endl;
	PressEnterKeyToContinue();
}

std::string FetchString(std::string MessagePrompt)
{
	ClearConsole();
	PrintProgramInformation();
	std::cout << MessagePrompt << ": ";
	std::string ResultData;
	std::cin >> ResultData;
	return ResultData;
}

std::string FetchStringWithSpaces(std::string MessagePrompt)
{
	ClearConsole();
	PrintProgramInformation();
	std::cout << MessagePrompt << ": ";
	std::string ResultData;
	std::getline(std::cin, ResultData);
	return ResultData;
}

void PrintProgramInformation()
{
	std::cout << "Projektowanie efektywnych algorytm\xA2w - zadanie projektowe nr 3" << std::endl;
	std::cout << "Autor: 2020 Micha\x88 Prochera (nr indeksu 248853)" << std::endl;
	std::cout << std::endl;
}

void PressEnterKeyToContinue()
{
	std::cout << std::endl;
	std::cout << "Naci\x98nij ENTER aby kontynowa\x86: ";
	while (_getch() != ENTER_KEY) return;
}

void ClearConsole()
{
	HANDLE ConsoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (ConsoleHandle == INVALID_HANDLE_VALUE) return;
	CONSOLE_SCREEN_BUFFER_INFO ConsoleInfo;
	if (!GetConsoleScreenBufferInfo(ConsoleHandle, &ConsoleInfo)) return;
	DWORD CharactersWritten = 0;
	COORD StartCordinates = {0, 0};
	DWORD CharacterCount = ConsoleInfo.dwSize.X * ConsoleInfo.dwSize.Y;
	if (!FillConsoleOutputCharacterW(ConsoleHandle, L' ', CharacterCount, 
		StartCordinates, &CharactersWritten)) 
	{
		return;
	}
	if (!FillConsoleOutputAttribute(ConsoleHandle, ConsoleInfo.wAttributes,	
		CharacterCount,	StartCordinates, &CharactersWritten)) 
	{
		return;
	}
	SetConsoleCursorPosition(ConsoleHandle, StartCordinates);
}