#include "Global.h"
#include "Benchmark.h"
#include "SalesmanData.h"
#include "LinkedList.h"
#include "SalesmanIO.h"
#include "SalesmanGenetic.h"

#include <limits>
#include <cmath>
#include <fstream>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#undef max

#define BENCHMARK_REPEATS_COUNT 10

#define BENCHMARK_TIME_LIMIT 30

#define BENCHMARK_FILE1_PATH "Testy\\ftv47.atsp"
#define BENCHMARK_FILE1_ROUTE_LEN 1776
#define BENCHMARK_FILE2_PATH "Testy\\ftv170.atsp"
#define BENCHMARK_FILE2_ROUTE_LEN 2755
#define BENCHMARK_FILE3_PATH "Testy\\rbg403.atsp"
#define BENCHMARK_FILE3_ROUTE_LEN 2465

#define BENCHMARK_OUT_DIRECTORY "Benchmark"

bool BenchmarkParameters(int PopulationSize, double CrossingRate, 
	double MutationRate, std::string ParameterString,
	std::ofstream& BenchmarkStream, std::string TestPrefix);
bool BenchmarkAlgorithm(
	int (*SalesmanFunction)(const SalesmanCities&, int, 
		int, double, double, LinkedList<int>& OutputRoute),
	const SalesmanCities& InputData, int PopulationSize, 
	double CrossingRate, double MutationRate, int BestRouteLength, 
	std::ofstream& BenchmarkStream, std::string BestRouteFilePath);
std::string GetFileNameWithoutExtension(std::string InputPath);
inline double GetRelativeErrorValue(int InputValue, int OptimalValue);

void BenchmarkSalesman()
{
	std::cout << "Testowanie jako\x98""ci rozwi\xA5za\xE4.." << std::endl;
	if (!CreateDirectoryA(BENCHMARK_OUT_DIRECTORY, NULL))
    {
		std::cout << "Wyst\xA5pi\x88 b\x88\xA5""d podczas"
			" tworzenia katalogu docelowego" << std::endl;
        return;
    }
	std::string OutputPathPrefix = BENCHMARK_OUT_DIRECTORY;
	OutputPathPrefix += "\\Results-";

	std::cout << "Testowanie parametru (1/2).." << std::endl;
	std::ofstream PopulationBenchStream(OutputPathPrefix + "P.txt");
	if (!PopulationBenchStream.is_open())
	{
		std::cout << "Wyst\xA5pi\x88 b\x88\xA5""d podczas otwierania pliku" << std::endl;
		return;
	}
	BenchmarkParameters(100, 0.8, 0.1, "100", PopulationBenchStream, "P");
	BenchmarkParameters(1000, 0.8, 0.1, "1000",  PopulationBenchStream, "P");
	BenchmarkParameters(10000, 0.8, 0.1, "10000", PopulationBenchStream, "P");
	PopulationBenchStream.close();

	std::cout << "Testowanie parametru (2/2).." << std::endl;
	std::ofstream MutationRateBenchStream(OutputPathPrefix + "MR.txt");
	if (!MutationRateBenchStream.is_open())
	{
		std::cout << "Wyst\xA5pi\x88 b\x88\xA5""d podczas otwierania pliku" << std::endl;
		return;
	}
	BenchmarkParameters(1000, 0.8, 0.02, "0.02", MutationRateBenchStream, "MR");
	BenchmarkParameters(1000, 0.8, 0.05, "0.05", MutationRateBenchStream, "MR");
	BenchmarkParameters(1000, 0.8, 0.1, "0.1", MutationRateBenchStream, "MR");
	MutationRateBenchStream.close();
	std::cout << "Pomy\x98lnie zako\xE4""czono testowanie jako\x98""ci rozwi\xA5za\xE4" << std::endl;
}

bool BenchmarkParameters(int PopulationSize, double CrossingRate, 
	double MutationRate, std::string ParameterString,
	std::ofstream& BenchmarkStream, std::string TestPrefix)
{
	SalesmanCities* FirstSalesmanData = nullptr;
	if (LoadSalesmanCitiesFromTSPLIBFile(BENCHMARK_FILE1_PATH, FirstSalesmanData) < 0)
	{
		std::cout << "Wyst\xA5pi\x88 b\x88\xA5""d podczas oczytywania pliku z danymi" << std::endl;
		return false;
	}
	SalesmanCities* SecondSalesmanData = nullptr;
	if (LoadSalesmanCitiesFromTSPLIBFile(BENCHMARK_FILE2_PATH, SecondSalesmanData) < 0)
	{
		std::cout << "Wyst\xA5pi\x88 b\x88\xA5""d podczas oczytywania pliku z danymi" << std::endl;
		delete FirstSalesmanData;
		return false;
	}
	SalesmanCities* ThirdSalesmanData = nullptr;
	if (LoadSalesmanCitiesFromTSPLIBFile(BENCHMARK_FILE3_PATH, ThirdSalesmanData) < 0)
	{
		std::cout << "Wyst\xA5pi\x88 b\x88\xA5""d podczas oczytywania pliku z danymi" << std::endl;
		delete SecondSalesmanData;
		delete FirstSalesmanData;
		return false;
	}

	std::string OutputPathPrefix = BENCHMARK_OUT_DIRECTORY;
	OutputPathPrefix += "\\";
	OutputPathPrefix +=	TestPrefix;
	OutputPathPrefix +=	"-P";
	OutputPathPrefix += std::to_string((long long int) PopulationSize);
	OutputPathPrefix +=	"-MR";
	OutputPathPrefix += std::to_string((long long int) (MutationRate * 1000));
	OutputPathPrefix += "-";
	BenchmarkStream << ParameterString << " ";

	std::cout << "Testowanie pliku (1/3).." << std::endl;
	BenchmarkAlgorithm(&SalesmanGeneticPMXSwapHeurSolver, *FirstSalesmanData, 
		PopulationSize, CrossingRate, MutationRate, BENCHMARK_FILE1_ROUTE_LEN,
		BenchmarkStream, OutputPathPrefix + "-F1-SWAP.txt");
	BenchmarkAlgorithm(&SalesmanGeneticPMXInsertHeurSolver, *FirstSalesmanData, 
		PopulationSize, CrossingRate, MutationRate, BENCHMARK_FILE1_ROUTE_LEN,
		BenchmarkStream, OutputPathPrefix + "-F1-INSERT.txt");

	std::cout << "Testowanie pliku (2/3).." << std::endl;
	BenchmarkAlgorithm(&SalesmanGeneticPMXSwapHeurSolver, *SecondSalesmanData, 
		PopulationSize, CrossingRate, MutationRate, BENCHMARK_FILE2_ROUTE_LEN,
		BenchmarkStream, OutputPathPrefix + "-F2-SWAP.txt");
	BenchmarkAlgorithm(&SalesmanGeneticPMXInsertHeurSolver, *SecondSalesmanData, 
		PopulationSize, CrossingRate, MutationRate, BENCHMARK_FILE2_ROUTE_LEN,
		BenchmarkStream, OutputPathPrefix + "-F2-INSERT.txt");

	std::cout << "Testowanie pliku (3/3).." << std::endl;
	BenchmarkAlgorithm(&SalesmanGeneticPMXSwapHeurSolver, *ThirdSalesmanData, 
		PopulationSize, CrossingRate, MutationRate, BENCHMARK_FILE3_ROUTE_LEN,
		BenchmarkStream, OutputPathPrefix + "-F3-SWAP.txt");
	BenchmarkAlgorithm(&SalesmanGeneticPMXInsertHeurSolver, *ThirdSalesmanData, 
		PopulationSize, CrossingRate, MutationRate, BENCHMARK_FILE3_ROUTE_LEN,
		BenchmarkStream, OutputPathPrefix + "-F3-INSERT.txt");

	BenchmarkStream << std::endl;

	delete ThirdSalesmanData;
	delete SecondSalesmanData;
	delete FirstSalesmanData;
	return true;
}

bool BenchmarkAlgorithm(
	int (*SalesmanFunction)(const SalesmanCities&, int, 
		int, double, double, LinkedList<int>& OutputRoute),
	const SalesmanCities& InputData, int PopulationSize, 
	double CrossingRate, double MutationRate, int BestRouteLength, 
	std::ofstream& BenchmarkStream, std::string BestRouteFilePath)
{
	LinkedList<int> OutputRoute;
	LinkedList<int> BestFoundRoute;
	int OutputRouteLength = 0;
	int BestFoundRouteLength = std::numeric_limits<int>::max();
	double TotalRelativeError = 0.0;
	for (int RepeatIndex = 0; 
		RepeatIndex < BENCHMARK_REPEATS_COUNT; ++RepeatIndex)
	{
		OutputRouteLength = SalesmanFunction(InputData, BENCHMARK_TIME_LIMIT, 
			PopulationSize, CrossingRate, MutationRate, OutputRoute);
		TotalRelativeError += GetRelativeErrorValue(OutputRouteLength, BestRouteLength);
		if (OutputRouteLength < BestFoundRouteLength)
		{
			BestFoundRoute.Clear();
			BestFoundRoute.Load(OutputRoute);
			BestFoundRouteLength = OutputRouteLength;
		}
		OutputRoute.Clear();
	}
	BenchmarkStream << (TotalRelativeError / BENCHMARK_REPEATS_COUNT) << " ";

	std::ofstream BestRouteFileStream(BestRouteFilePath);//plik+cnyt
	if (!BestRouteFileStream.is_open())
	{
		std::cout << "Wyst\xA5pi\x88 b\x88\xA5""d podczas otwierania pliku: "
			<< BestRouteFilePath << std::endl;
		return false;
	}
	std::streambuf* ConsoleOutputBuffer = std::cout.rdbuf();
    std::cout.rdbuf(BestRouteFileStream.rdbuf());
	std::cout << "Najlepsza znaleziona trasa:" << std::endl;
	PrintSalesmanRoute(BestFoundRoute, BestFoundRouteLength);
	std::cout.rdbuf(ConsoleOutputBuffer);
	BestRouteFileStream.close();
	return true;
}

double GetRelativeErrorValue(int InputValue, int OptimalValue)
{
	return ((double) (abs(InputValue - OptimalValue) * 100)) / OptimalValue;
}
