#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "Exception.h"
#include "LinkedList.h"

/*
* Modu� SalesmanTabu
*
* Zawiera implementacje metaheurystyki
* przeszukiwania z zakazami (ang. Tabu Search)
* przystosowanej do rozwi�zywania problemu
* komiwoja�era, wykorzystuj�ce r�ne rozdzaje 
* s�siedztwa.
*/

//Funkcja zawieraj�ca implementacj� algorytmu
//przeszukiwania z zakazami dla s�siedztwa 
//typu swap - zamiana 2 wierzcho�k�w w cyklu
int SalesmanTabuSearchSwapHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, LinkedList<int>& OutputRoute);

//Funkcja implementuj�ca algorytm przeszukiwania 
//z zakazami dla s�siedztwa typu insert, czyli 
//wstawienie jednego wierzcho�ka przed drugi
int SalesmanTabuSearchInsertHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, LinkedList<int>& OutputRoute);

//Implementacja Tabu Search dla problemu aTSP
//oraz s�dziedztwa invert - odwr�cenie kolejno�ci
//po��cze� pomi�dzy 2 wierzcho�kami
int SalesmanTabuSearchInvertHeurSolver(const SalesmanCities& InputData, 
	int SecondsLimit, LinkedList<int>& OutputRoute);