#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "LinkedList.h"

/*
* Modu� SalesmanNearest
*
* Zawiera implementacj� algorytmu najbli�szego
* s�siada generuj�cego przybli�one rozwi�zanie 
* aTSP, w spos�b zach�anny.
*/

//Funkcja realizuj�ca algorytm najbli�szego s�siada
int SalesmanNearestHeurSolver(const SalesmanCities& InputData, 
	int StartCity, LinkedList<int>& OutputRoute);