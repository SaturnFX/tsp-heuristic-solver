#pragma once
#include "Global.h"
#include "SalesmanData.h"
#include "LinkedList.h"

/*
* Modu� SalesmanAnnealing
*
* Zawiera funkcje s�u��ce do uzyskiwania
* przybli�onych rozwi�za� asymetrycznego
* problemu komiwoja�era z u�yciem metody
* symulowanego wy�arzania
*/

//Funkcja realizuj�ca metod� symulowanego 
//wy�arzania dla aTSP. U�yto tutaj s�siedztwa
//typu swap - zamiana, sch�adzanie odbywa
//si� geometrycznie, a za temperatur� 
//pocz�tkow� przyjmowana jest waga MST
//wej�ciowego grafu (dla jego 
//nieskierowanej interpretacji).
int SalesmanAnnealingHeurSolver(const SalesmanCities& InputData, 
	double TemperatureStep, int SecondsLimit, LinkedList<int>& OutputRoute);

//Procedura kalibruj�ca algorytm SW dla 
//okre�lonego rozmiaru, czyli przeprowadzaj�ca 
//pomiar wydajno�ci dzia�ania algorytmu i 
//zapisuj�ca wyniki do u�ycia podczas jego
//kolejnego uruchomienia. Je�li w�wczas
//na dzia�anie algorytmu jest przeznaczone
//mniej czasu ni� jest potrzebne, aby 
//temperatura ko�cowa osi�gne�a warto��
//blisk� zeru, wtedy temperatura pocz�tkowa
//zmniejszana jest do takiego poziomu,
//�eby ten warunek by� zawsze spe�niony.
//Dzi�ki temu algorytm nie ko�czy pracy
//w etapie "eksploracji", a wykonuje
//jej tyle, na ile pozwala czas i przechodzi
//do "eksploatacji", aby maksymalnie poprawi�
//znalezione ju� rozwi�zania, co przek�ada si�
//na lepsze wyniki algorytmu.
void CalibrateSalesmanAnnealing(int CitiesCount);

//Procedura resetuj�ca stan kalibracji.
//Po jej wykonaniu algorytm ponownie zachowuje
//si� jakby nie wykonano kalibracji.
void ResetSalesmanAnnealingCalibration();